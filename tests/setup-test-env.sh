#!/bin/bash

set -e

PACKAGE="$1"
PODIR="$2"

if [ ! -d locales ]; then \
    mkdir -p locales; \
    for po in fr zh zh_TW; do \
        mkdir -p "locales/${po}/LC_MESSAGES"; \
        msgfmt "${PODIR}/${po}.po" -o "locales/${po}/LC_MESSAGES/${PACKAGE}.mo"; \
    done; \
fi;
