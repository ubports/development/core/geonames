pkg_check_modules(GTK REQUIRED gtk+-3.0)

add_executable(geonames-demo
    geonames-demo.c
)

target_link_libraries(geonames-demo
    ${PROJECT_NAME}
    ${GTK_LIBRARIES}
)

target_include_directories(geonames-demo PRIVATE
    ${PROJECT_NAME}
    ../src

    ${GTK_INCLUDE_DIRS}
)

install(TARGETS geonames-demo RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
