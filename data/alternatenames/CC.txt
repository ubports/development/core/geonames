755572	1547348		Keeling Islands						
755573	1547348		Cocos-Keeling Islands						
1984353	1547348	en	Cocos						
1984354	1547348	de	Kokosinseln						
1984355	1547348	es	Islas Cocos						
1984356	1547348	pl	Wyspy Kokosowe						
1984357	1547348	fr	Îles Cocos						
1984358	1547348	bs	Kokosova ostrva						
1984359	1547348	ca	Illes Cocos						
1984360	1547348	cs	Kokosové ostrovy						
1984361	1547348	da	Cocosøerne						
1984362	1547348	el	Νησιά Κόκος						
1984363	1547348	eo	Kokosinsuloj						
1984364	1547348	fi	Kookossaaret						
1984365	1547348	gl	Illas Cocos						
1984366	1547348	he	איי קוקוס						
1984367	1547348	hu	Kókusz						
1984368	1547348	id	Kepulauan Cocos						
1984369	1547348	is	Kókoseyjar						
1984370	1547348	it	Isole Cocos						
1984371	1547348	ja	ココス諸島						
1984372	1547348	ko	코코스 제도						
1984373	1547348	kw	Ynysow Cocos						
1984374	1547348	ms	Kepulauan Cocos						
1984375	1547348	nl	Cocoseilanden						
1984376	1547348	no	Kokosøyene						
1984377	1547348	pt	Ilhas Cocos						
1984378	1547348	ro	Insulele Cocos						
1984379	1547348	ru	Кокосовые острова						
1984380	1547348	hbs	Kokosovi otoci						
1984381	1547348	sk	Kokosové ostrovy						
1984382	1547348	sv	Kokosöarna						
1984383	1547348	tr	Cocos Adaları						
1984384	1547348	zh	科科斯						
2427746	1547348	lt	Kokosų (Kilingo) Salos						
2427747	1547348	lv	Kokosu (Kīlinga) Salas						
3050579	1547348	link	https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BA%D0%BE%D1%81%D0%BE%D0%B2%D1%8B%D0%B5_%D0%BE%D1%81%D1%82%D1%80%D0%BE%D0%B2%D0%B0						
4330214	1547348		Cocos Islands						
13287028	1547348	iata	CCK						
13734324	1547348	unlc	CCCCK						
15772656	1547350	wkdt	Q22637269						
15969138	1547350		Workhouse Island						
755574	1547351		Ross						
755575	1547351	ms	Pulo Panjang						
755576	1547351		Ross Island						
2935148	1547351	link	https://en.wikipedia.org/wiki/West_Island%2C_Cocos_%28Keeling%29_Islands						
4330215	1547351		West Island						
11472873	1547351	en	West Island, Cocos						
11472874	1547351	ar	ويست آيلند						
11472875	1547351	az	Uest Aylend						
11472876	1547351	be	Востраў Вест-Айленд						
11472877	1547351	bg	Уест Айлънд						
11472878	1547351	eo	Okcidenta Insulo						
11472879	1547351	fa	وست آیلند، جزایر کوکوس						
11472880	1547351	hi	पश्चिम द्वीप						
11472881	1547351	ja	ウェスト島						
11472882	1547351	ka	ვესტ-აილენდი						
11472883	1547351	ko	웨스트 섬						
11472884	1547351	mk	Вест Ајленд						
11472885	1547351	pa	ਪੱਛਮੀ ਟਾਪੂ, ਕੋਕੋਸ						
11472886	1547351	sr	Вест Ајленд						
11472887	1547351	ta	மேற்குத் தீவு						
11472888	1547351	th	เวสต์ไอแลนด์						
11472889	1547351	tr	West Adası						
11472890	1547351	ur	ویسٹ آئلینڈ، جزائر کوکوس						
11472891	1547351	vi	West Island, Quần đảo Cocos						
11472892	1547351	war	West Island, Kapuropud-an Cocos						
11472893	1547351	ru	Уэст-Айленд						
11472894	1547351	zh	西島						
13359483	1547351	coa	Pulu Panjang						
13359484	1547351		West Is Cocos (Keeling)						
755577	1547354		Tanjong Batu Kapala						
4330216	1547354		Ujong Tanjong						
8108859	1547355		South Lagoon						
8108860	1547355		Telok Kambing						
755578	1547356		South Keeling						
755579	1547356		South Keeling Island						
4330217	1547356		South Keeling Islands						
755580	1547357		Selma Island						
755581	1547357		Selma						
755582	1547357		Selima Island						
755583	1547357		Pulo Atas						
4330218	1547357		South Island						
7119139	1547358		Pulu Siput						
7119140	1547358		Pulo Siput						
15802178	1547358	wkdt	Q22620382						
8108861	1547361		Tanjong Pugi						
8108862	1547361		Tanjong Puji						
7119141	1547364		Pulu Pandan						
7119142	1547364		Pulo Pandang						
15898567	1547364	wkdt	Q22620366						
7119143	1547365		North Lagoon						
7119144	1547365		Teluk Jambu						
755584	1547366		North Keeling						
4330219	1547366		North Keeling Island						
7158154	1547366	link	https://en.wikipedia.org/wiki/North_Keeling						
755585	1547367		West Cay						
4330220	1547367		Pulo Maria						
7119145	1547367		Pulu Maria						
15399469	1547367	wkdt	Q22620354						
755586	1547368		Klapa Sato						
4330221	1547368		Pulo Klapa Satu						
15785353	1547368	wkdt	Q22620107						
8108863	1547369		Pulo Kambing						
8108864	1547369		Pulo Kambling						
15696191	1547369	wkdt	Q22699773						
7119146	1547370		Pulu Jembatan						
7119147	1547370		Pulo Jambatan						
15763650	1547370	wkdt	Q22620326						
755587	1547371		Pulo Luar						
4330222	1547371		Horsburgh Island						
6889995	1547371	link	https://en.wikipedia.org/wiki/Horsburgh_Island						
15217330	1547371	wkdt	Q1061922						
755588	1547372		New Selma Island						
755589	1547372		Clunies Ross Island						
755590	1547372		Clunies Ross						
755591	1547372		New Selma						
4330223	1547372		Home Island						
7169467	1547372	link	https://en.wikipedia.org/wiki/Home_Island						
13359479	1547372		Pulu Selma						
755592	1547373		Clunie Island						
4330224	1547373		Pulo Gangsa						
8108865	1547375		Direction Island						
8108866	1547375		Pulo Tikus						
8673387	1547375	link	https://en.wikipedia.org/wiki/Direction_Island_%28Cocos_%28Keeling%29_Islands%29						
755594	1547376		Cocos Islands						
2256611	1547376	fr	Territoire des îles Cocos						
2256612	1547376		Keeling Islands						
2256613	1547376	fr	Îles Cocos (Keeling)						
2256654	1547376	fr	Îles Cocos	1					
2419961	1547376	am	ኮኮስ(ኬሊንግ) ደሴቶች	1					
2419962	1547376	ar	جزر كوكوس (كيلينغ)	1					
2419963	1547376	be	Какосавыя (Кілінг) астравы	1					
2419964	1547376	bg	Кокосови острови (острови Кийлинг)	1					
2419965	1547376	bn	কোকোস (কিলিং) দ্বীপপুঞ্জ	1					
2419966	1547376	ca	Illes Cocos	1					
2419967	1547376	cs	Kokosové ostrovy	1					
2419968	1547376	cy	Ynysoedd Cocos (Keeling)	1					
2419969	1547376	da	Cocosøerne	1					
2419970	1547376	de	Kokosinseln	1					
2419971	1547376	el	Νήσοι Κόκος (Κίλινγκ)	1					
2419972	1547376	en	Cocos (Keeling) Islands	1					
2419973	1547376	es	Islas Cocos	1					
2419974	1547376	et	Kookossaared	1					
2419975	1547376	eu	Cocos (Keeling) uharteak	1					
2419976	1547376	fa	جزایر کوکوس	1					
2419977	1547376	fi	Kookossaaret (Keelingsaaret)	1					
2419979	1547376	ga	Oileáin Cocos (Keeling)	1					
2419980	1547376	gl	Illas Cocos (Keeling)	1					
2419981	1547376	he	איי קוקוס (קילינג)	1					
2419982	1547376	hi	कोकोस (कीलिंग) द्वीपसमूह	1					
2419983	1547376	hr	Kokosovi (Keelingovi) otoci	1					
2419984	1547376	hu	Kókusz (Keeling)-szigetek	1					
2419985	1547376	id	Kepulauan Cocos (Keeling)	1					
2419986	1547376	is	Kókoseyjar (Keeling)	1					
2419987	1547376	it	Isole Cocos (Keeling)	1					
2419988	1547376	ja	ココス(キーリング)諸島	1					
2419989	1547376	ko	코코스 제도	1					
2419990	1547376	lt	Kokosų (Kilingo) Salos	1					
2419991	1547376	lv	Kokosu (Kīlinga) salas	1					
2419992	1547376	mk	Кокосови (Килиншки) Острови	1					
2419993	1547376	ml	കോക്കസ് (കീലിംഗ്) ദ്വീപുകൾ	1					
2419994	1547376	ms	Kepulauan Cocos (Keeling)	1					
2419995	1547376	mt	Gżejjer Cocos (Keeling)	1					
2419996	1547376	nb	Kokosøyene	1					
2419997	1547376	nl	Cocoseilanden	1					
2419998	1547376	nn	Kokosøyane	1					
2419999	1547376	pl	Wyspy Kokosowe	1					
2420000	1547376	pt	Ilhas Cocos (Keeling)	1					
2420001	1547376	ro	Insulele Cocos (Keeling)	1					
2420002	1547376	ru	Кокосовые о-ва	1					
2420003	1547376	sk	Kokosové ostrovy	1					
2420004	1547376	sl	Kokosovi otoki	1					
2420005	1547376	sr	Кокосова (Килингова) Острва	1					
2420006	1547376	sv	Kokosöarna	1					
2420007	1547376	th	หมู่เกาะโคโคส (คีลิง)	1					
2420008	1547376	tr	Cocos (Keeling) Adaları	1					
2420009	1547376	uk	Кокосові (Кілінг) Острови	1					
2420010	1547376	ur	کوکوس (کیلنگ) جزائر	1					
2420011	1547376	vi	Quần đảo Cocos (Keeling)	1					
2420012	1547376	zh-TW	科科斯群岛	1					
2920553	1547376	link	https://en.wikipedia.org/wiki/Cocos_%28Keeling%29_Islands						
3332189	1547376	link	https://en.wikipedia.org/wiki/Cocos_(Keeling)_Islands						
7085685	1547376	af	Kokoseilande	1					
7085686	1547376	az	Kokos (Kilinq) adaları	1					
7085687	1547376	br	Inizi Kokoz	1					
7085688	1547376	bs	Kokosova (Keelingova) ostrva	1					
7085689	1547376	ee	Kokos (Kiling) fudomekpo nutome	1					
7085690	1547376	fo	Kokosoyggjar	1					
7085691	1547376	gu	કોકોઝ (કીલીંગ) આઇલેન્ડ્સ	1					
7085692	1547376	kn	ಕೊಕೊಸ್ (ಕೀಲಿಂಗ್) ದ್ವೀಪಗಳು	1					
7085693	1547376	mr	कोकोस (कीलिंग) बेटे	1					
7085694	1547376	my	ကိုကိုးကျွန်း	1					
7085695	1547376	ne	कोकोस (किलिंग) टापुहरु	1					
7085696	1547376	or	କୋକୋସ୍ (କୀଲିଂ) ଦ୍ଵୀପପୁଞ୍ଜ	1					
7085697	1547376	rm	Inslas Cocos	1					
7085698	1547376	se	Cocos-sullot	1					
7085699	1547376	si	කොකෝස් දූපත්	1					
7085700	1547376	sw	Visiwa vya Cocos (Keeling)	1					
7085701	1547376	ta	கோகோஸ் (கீலிங்) தீவுகள்	1					
7085702	1547376	te	కోకోస్ (కీలింగ్) దీవులు	1					
7085703	1547376	ti	ኮኮስ ኬሊንግ ደሴቶች	1					
7085704	1547376	to	ʻOtumotu Koko	1					
7085705	1547376	zu	i-Cocos (Keeling) Islands	1					
16490090	1547376	zh-CN	科科斯（基林）群岛						
16925691	1547376	zh-Hant	科克斯（基靈）群島	1					
16932503	1547376	as	কোকোচ (কীলিং) দ্বীপপুঞ্জ	1					
16932504	1547376	ce	Кокосийн гӀайренаш	1					
16932505	1547376	dz	ཀོ་ཀོས་གླིང་ཚོམ	1					
16932506	1547376	fy	Kokosilanen	1					
16932507	1547376	gd	Na h-Eileanan Chocos (Keeling)	1					
16932508	1547376	ha	Tsibirai Cocos (Keeling)	1					
16932509	1547376	hy	Կոկոսյան (Քիլինգ) կղզիներ	1					
16932510	1547376	ig	Agwaetiti Cocos (Keeling)	1					
16932511	1547376	jv	Kapuloan Cocos (Keeling)	1					
16932512	1547376	ka	ქოქოსის (კილინგის) კუნძულები	1					
16932513	1547376	kk	Кокос (Килинг) аралдары	1					
16932514	1547376	kl	Cocos qeqertaq	1					
16932515	1547376	km	កោះ​កូកូស (គីលីង)	1					
16932516	1547376	ks	کوکَس کیٖلِنگ جٔزیٖرٕ	1					
16932517	1547376	ky	Кокос (Килинг) аралдары	1					
16932518	1547376	lb	Kokosinselen	1					
16932519	1547376	ln	Bisanga Kokos	1					
16932520	1547376	lo	ຫມູ່ເກາະໂກໂກສ	1					
16932521	1547376	mn	Кокос (Кийлинг) арлууд	1					
16932522	1547376	no	Kokosøyene	1					
16932523	1547376	pa	ਕੋਕੋਸ (ਕੀਲਿੰਗ) ਟਾਪੂ	1					
16932524	1547376	ps	کوکوز (کيلنګ) ټاپوګان	1					
16932525	1547376	qu	Islas Cocos	1					
16932526	1547376	sd	ڪوڪوس ٻيٽ	1					
16932527	1547376	so	Jasiiradda Kookoos	1					
16932528	1547376	sq	Ishujt Kokos	1					
16932529	1547376	tg	Ҷазираҳои Кокос (Килинг)	1					
16932530	1547376	tk	Kokos (Kiling) adalary	1					
16932531	1547376	tt	Кокос (Килинг) утраулары	1					
16932532	1547376	ug	كوكوس (كىلىڭ) ئاراللىرى	1					
16932533	1547376	uz	Kokos (Kiling) orollari	1					
16932534	1547376	wo	Duni Koko (Kilin)	1					
16932535	1547376	yo	Erékùsù Cocos (Keeling)	1					
16932536	1547376	zh	科科斯（基林）群岛	1					
755595	1547377		Gooseberry Island						
4330225	1547377		Pulo Cheplok						
755596	1547378		Keche Pecha						
4330226	1547378		Burton Point						
755597	1547379		Burial Island						
4330227	1547379		Pulo Blan Madar						
8108867	1547379		Pulu Belan Madar						
15707106	1547379	wkdt	Q22620309						
755598	1547380		East Cay						
4330228	1547380		Pulo Blan						
8108868	1547380		Pulu Belan						
13359672	1547382	link	https://en.wikipedia.org/wiki/Bantam,_Cocos_(Keeling)_Islands						
755660	1547412		Pulo Ampang Kechil						
4722213	1547412		Pulu Ampang Kechil						
15818253	1547412	wkdt	Q22620293						
755661	1547413		Pulo Ampang						
4722214	1547413		Pulu Ampang						
755662	1547414		Pulo Wa-idas						
4722215	1547414		Pulu Wa-idas						
755663	1547415		Pulo Blukok						
4722216	1547415		Pulu Blekok						
755664	1547416		Pulo Kambang						
4722217	1547416		Pulu Kembang						
755665	1547417		Pulo Labu						
4722218	1547417		Pulu Labu						
15577377	1547417	wkdt	Q22620346						
755666	1547418		Telok Grongeng						
4722219	1547418		Telok Gronjeng						
8111752	1547418		Telok Gerongjeng						
755667	1547419		Telok Sebrang						
4722220	1547419		Telok Sabrang						
8111753	1547419		Telok Semberang						
755668	1547420		Ujong Pulo Jau						
4722221	1547420		Ujong Pulu Jau						
755669	1547426		Teluk Jambu						
4726347	1547426		North Lagoon						
1888047	6301324	icao	YPCC						
1890371	6301324	iata	CCK						
5806419	6301324	link	https://en.wikipedia.org/wiki/Cocos_%28Keeling%29_Islands_Airport						
5424025	7304591	link	https://en.wikipedia.org/wiki/West_Island,_Cocos_(Keeling)_Islands						
13895952	7304591	ko	웨스트 섬						
7287326	8173550	en	Cocos Island High School						
7293866	8178720	en	Pulu Wak Banka						
15470473	8178720	wkdt	Q22620403						
7293867	8178721	en	Pulu Pasir						
15395650	8178721	wkdt	Q22620375						
7293868	8178722	en	Pulu Belan						
7397467	8215984	en	Trannies Beach						
15570929	8215984	wkdt	Q22521524						
7399001	8216864	en	Cocos Island Golf Club						
7675811	8310002	en	Port Refuge						
15705979	8514707	wkdt	Q22382341						
16055872	8514707		Ujong Pulo Dekat						
8136001	8515502		Great House						
8136002	8515502		Oceania House						
