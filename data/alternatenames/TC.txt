11912730	3576903	en	Windward Going Through						
11912731	3576905	en	Whitby						
11665066	3576908	en	West Sand Spit						
11912732	3576909	en	West Road						
1455526	3576911		Belle Isle						
1455527	3576911		West Caicos						
4314602	3576911		West Caicos Island						
6890164	3576911	link	https://en.wikipedia.org/wiki/West_Caicos						
11912733	3576912	en	Watering Point						
1455528	3576914		Iles Turques						
1455529	3576914		Turk Islands						
1627200	3576914	ca	Illes Turks						
2296510	3576914	fr	turks et caicos	1	1				
4314603	3576914		Turks Islands						
1455530	3576915		Grand Turk Passage						
1455531	3576915		Turk Passage						
1455532	3576915		Turks Islands Passage						
4314604	3576915		Turks Island Passage						
1627195	3576916	ca	Illes Turks i Caicos	1					
2256639	3576916	fr	Îles Turques-et-Caïques	1					
2418104	3576916	am	የቱርኮችና የካኢኮስ ደሴቶች	1					
2418105	3576916	ar	جزر توركس وكايكوس	1					
2418106	3576916	be	Астравы Цёркс і Кайкас	1					
2418107	3576916	bg	острови Търкс и Кайкос	1					
2418108	3576916	bn	তুর্কস ও কাইকোস দ্বীপপুঞ্জ	1					
2418109	3576916	cs	Turks a Caicos	1					
2418110	3576916	cy	Ynysoedd Turks a Caicos	1					
2418111	3576916	da	Turks- og Caicosøerne	1					
2418112	3576916	de	Turks- und Caicosinseln	1					
2418113	3576916	el	Νήσοι Τερκς και Κάικος	1					
2418114	3576916	en	Turks and Caicos Islands	1					
2418115	3576916	es	Islas Turcas y Caicos	1					
2418116	3576916	et	Turks ja Caicos	1					
2418117	3576916	eu	Turk eta Caico uharteak	1					
2418118	3576916	fa	جزایر تورکس و کایکوس	1					
2418119	3576916	fi	Turks- ja Caicossaaret	1					
2418120	3576916	fr	Îles Turks et Caïques						
2418121	3576916	ga	Oileáin na dTurcach agus Caicos	1					
2418122	3576916	gl	Illas Turks e Caicos	1					
2418123	3576916	he	איי טרקס וקייקוס	1					
2418124	3576916	hi	तुर्क और कैकोज़ द्वीपसमूह	1					
2418125	3576916	hr	Otoci Turks i Caicos	1					
2418126	3576916	hu	Turks- és Caicos-szigetek	1					
2418127	3576916	ia	Insulas Turcos e Caicos	1					
2418128	3576916	id	Kepulauan Turks dan Caicos	1					
2418129	3576916	is	Turks- og Caicoseyjar	1					
2418130	3576916	it	Isole Turks e Caicos	1					
2418131	3576916	ja	タークス・カイコス諸島	1					
2418132	3576916	ko	터크스 케이커스 제도	1					
2418133	3576916	lt	Terkso ir Kaikoso Salos	1					
2418134	3576916	lv	Tērksas un Kaikosas salas	1					
2418135	3576916	mk	Острови Туркс и Каикос	1					
2418136	3576916	ms	Kepulauan Turks dan Caicos	1					
2418137	3576916	mt	il-Gżejjer Turks u Caicos	1					
2418138	3576916	nb	Turks- og Caicosøyene	1					
2418139	3576916	nl	Turks- en Caicoseilanden	1					
2418140	3576916	nn	Turks- og Caicosøyane	1					
2418141	3576916	pl	Turks i Caicos	1					
2418142	3576916	pt	Ilhas Turcas e Caicos	1					
2418143	3576916	ro	Insulele Turks și Caicos	1					
2418144	3576916	ru	о-ва Тёркс и Кайкос	1					
2418145	3576916	se	Turks ja Caicos-sullot	1					
2418146	3576916	sk	Turks a Caicos	1					
2418147	3576916	sl	Otoki Turks in Caicos	1					
2418148	3576916	sr	Острва Туркс и Каикос	1					
2418149	3576916	sv	Turks- och Caicosöarna	1					
2418150	3576916	th	หมู่เกาะเติกส์และหมู่เกาะเคคอส	1					
2418151	3576916	tr	Turks ve Caicos Adaları	1					
2418152	3576916	uk	Острови Теркс і Кайкос	1					
2418153	3576916	ur	ٹرکس اور کیکوس جزائر	1					
2418154	3576916	vi	Quần đảo Turks và Caicos	1					
2418155	3576916	zh	特克斯和凯科斯群岛	1					
2426521	3576916	ca	Turks i Caicos	1	1				
4314605	3576916		Turks and Caicos Islands						
5423952	3576916	link	https://en.wikipedia.org/wiki/Turks_and_Caicos_Islands						
7091035	3576916	af	Turks- en Caicoseilande	1					
7091036	3576916	ak	Turks ne Caicos Islands	1					
7091037	3576916	az	Törks və Kaykos adaları	1					
7091038	3576916	bm	Turiki Gun ni Kayiki	1					
7091039	3576916	br	Inizi Turks ha Caicos	1					
7091040	3576916	bs	Ostrva Turks i Kaikos	1					
7091041	3576916	ee	Tɛks kple Kaikos ƒudomekpowo nutome	1					
7091042	3576916	ff	Duuɗe Turke e Keikoos	1					
7091043	3576916	fo	Turks- og Caicosoyggjar	1					
7091044	3576916	gu	તુર્ક્સ અને કેકોઝ આઇલેન્ડ્સ	1					
7091045	3576916	ha	Turkis Da Tsibiran Kaikwas	1					
7091046	3576916	ki	Visiwa vya Turki na Kaiko	1					
7091047	3576916	kn	ಟರ್ಕ್ಸ್ ಮತ್ತು ಕೈಕೋಸ್ ದ್ವೀಪಗಳು	1					
7091048	3576916	lg	Bizinga by’eTaaka ne Kayikosi	1					
7091049	3576916	ln	Bisanga bya Turki mpé Kaiko	1					
7091050	3576916	lu	Lutanda lua Tuluki ne Kaiko	1					
7091051	3576916	mg	Nosy Turks sy Caïques	1					
7091052	3576916	ml	ടർക്ക്‌സും കെയ്‌ക്കോ ദ്വീപുകളും	1					
7091053	3576916	mr	टर्क्स आणि कैकोस बेटे	1					
7091054	3576916	nd	Turks and Caicos Islands	1					
7091055	3576916	ne	तुर्क र काइकोस टापु	1					
7091056	3576916	or	ତୁର୍କସ୍‌ ଏବଂ କାଇକୋସ୍‌ ଦ୍ୱୀପପୁଞ୍ଜ	1					
7091057	3576916	rm	Inslas Turks e Caicos	1					
7091058	3576916	rn	Amazinga ya Turkisi na Cayikosi	1					
7091059	3576916	sg	Âzûâ Turku na Kaîki	1					
7091060	3576916	si	ටර්ක්ස් සහ කයිකොස් දූපත්	1					
7091061	3576916	sn	Zvitsuwa zveTurk neCaico	1					
7091062	3576916	so	Turks & Kaikos Island	1					
7091063	3576916	sw	Visiwa vya Turks na Caicos	1					
7091064	3576916	ta	டர்க்ஸ் & கைகோஸ் தீவுகள்	1					
7091065	3576916	te	టర్క్స్ మరియు కైకోస్ దీవులు	1					
7091066	3576916	ti	ደሴታት ቱርክን ካይኮስን	1					
7091067	3576916	to	ʻOtumotu Tuki mo Kaikosi	1					
7091068	3576916	yo	Tọọki ati Etikun Kakọsi	1					
7091069	3576916	zu	i-Turks ne-Caicos Islands	1					
10581205	3576916	fr	îles Turks-et-Caïcos						
13337916	3576916	ko	터크스케이커스						
16925866	3576916	zh-Hant	土克斯及開科斯群島	1					
16931401	3576916	as	টাৰ্কছ অৰু কেইক’ছ দ্বীপপুঞ্জ	1					
16931402	3576916	ce	Тёркс а, Кайкос а гӀайренаш	1					
16931403	3576916	dz	ཏུརྐས྄་ ཨེནཌ་ ཀ་ཀོས་གླིང་ཚོམ	1					
16931404	3576916	fy	Turks- en Caicoseilannen	1					
16931405	3576916	gd	Na h-Eileanan Turcach is Caiceo	1					
16931406	3576916	hy	Թըրքս և Կայկոս կղզիներ	1					
16931407	3576916	ig	Agwaetiti Turks na Caicos	1					
16931408	3576916	jv	Turks lan Kapuloan Kaikos	1					
16931409	3576916	ka	თერქს-ქაიქოსის კუნძულები	1					
16931410	3576916	kk	Теркс және Кайкос аралдары	1					
16931411	3576916	km	កោះ​ទួគ និង កៃកូស	1					
16931412	3576916	ks	تُرُک تہٕ کیکوس جٔزیٖرٕ	1					
16931413	3576916	ku	Giravên Turk û Kaîkos	1					
16931414	3576916	ky	Түркс жана Кайкос аралдары	1					
16931415	3576916	lb	Turks- a Caicosinselen	1					
16931416	3576916	lo	ໝູ່ເກາະ ເທີກ ແລະ ໄຄໂຄສ	1					
16931417	3576916	mn	Турк ба Кайкосын Арлууд	1					
16931418	3576916	my	တခ်စ်နှင့်ကာအီကိုစ်ကျွန်းစု	1					
16931419	3576916	no	Turks- og Caicosøyene	1					
16931420	3576916	pa	ਟੁਰਕਸ ਅਤੇ ਕੈਕੋਸ ਟਾਪੂ	1					
16931421	3576916	ps	د ترکیې او کیکاسو ټاپو	1					
16931422	3576916	qu	Islas Turcas y Caicos	1					
16931423	3576916	sd	ترڪ ۽ ڪيڪوس ٻيٽ	1					
16931424	3576916	sq	Ishujt Turks dhe Kaikos	1					
16931425	3576916	tg	Ҷазираҳои Теркс ва Кайкос	1					
16931426	3576916	tk	Terks we Kaýkos adalary	1					
16931427	3576916	tt	Теркс һәм Кайкос утраулары	1					
16931428	3576916	ug	تۇركس ۋە كايكوس ئاراللىرى	1					
16931429	3576916	uz	Turks va Kaykos orollari	1					
16931430	3576916	wo	Duni Tirk ak Kaykos	1					
11912734	3576917	en	Tucker Rock						
1455533	3576920		Centry						
4314606	3576920		Toney Rock						
1455534	3576922		The Bight						
4314607	3576922		The Bight Settlements						
11912735	3576924	en	Sugar Cane Mill						
1455535	3576925		Stubb Cay						
4314608	3576925		Stubbs Cay						
11912736	3576926	en	Stake Bank						
1455536	3576927		West Caicos Reefs						
4314609	3576927		Southwest Reefs						
11806855	3576928		South West Point						
11806856	3576928		Southwest Point						
11912737	3576930	en	Southern Bush Bay						
1455537	3576931		Caïque du Sud						
1455538	3576931		South Caicos						
4314610	3576931		South Caicos Island						
6889442	3576931	link	https://en.wikipedia.org/wiki/South_Caicos						
12252185	3576931	iata	XSC						
13800112	3576931	unlc	TCXSC						
1455539	3576933		Six Hill Cay						
4314611	3576933		Six Hill Cays						
1455540	3576934		Silver passage						
4314612	3576934		Silver Bank Passage						
11806857	3576936		Sellar’s Cut						
11806858	3576936		Seller’s Cut						
1455541	3576937		Cayes de Seal						
4314613	3576937		Seal Cays						
1455542	3576939		Caye de Sel						
4314614	3576939		Salt Cay						
6889334	3576939	link	https://en.wikipedia.org/wiki/Salt_Cay%2C_Turks_Islands						
8514714	3576939	en	Salt Cay						
8514715	3576939	ar	مدينة الملح، جزر توركس						
8514716	3576939	es	Cayo Sal						
8514717	3576939	fr	Salt Cay						
8515220	3576939	iata	SLX						
13800111	3576939	unlc	TCSLX						
11912738	3576940	en	Sail Rock Island						
11912739	3576943	en	Pumpkin Bluff Pond						
2986173	3576944	link	https://en.wikipedia.org/wiki/Providenciales						
4314615	3576944	en	Providenciales Island						
4314617	3576944	en	Providence Island						
4314618	3576944		Blue Hills						
8522320	3576944	iata	PLS						
8522321	3576944	wuu	普羅維登西亞萊斯島						
8522322	3576944	fr	Île de Providenciales						
8554641	3576944		Providenciales						
13800110	3576944	unlc	TCPLS						
11912740	3576946	en	Plandon Cay						
6889375	3576947	link	https://en.wikipedia.org/wiki/Pine_Cay						
6923172	3576947		Pine Cay						
12252106	3576947	iata	PIC						
13344358	3576947	ko	파인 케이						
13800109	3576947	unlc	TCPIC						
11912741	3576949	en	Penniston Cay						
15228370	3576949	wkdt	Q16510066						
1455546	3576950		Bird Island						
4314619	3576950		Pear Cay						
15804280	3576951	wkdt	Q49668435						
15959511	3576951		Pear Cay						
1455547	3576952		Caye Parrot						
4314620	3576952		Parrot Cay						
6889675	3576952	link	https://en.wikipedia.org/wiki/Parrot_Cay						
11912742	3576953	en	Over Back The Damp						
11912743	3576954	en	Ocean Hole						
11912744	3576956	en	North Creek						
1455548	3576957		North Caicos						
1455549	3576957		Caïque du Nord						
4314621	3576957		North Caicos Island						
6889955	3576957	link	https://en.wikipedia.org/wiki/North_Caicos						
12252182	3576957	iata	NCA						
13344357	3576957	ko	노스 카이코스						
13800108	3576957	unlc	TCNCA						
11912745	3576958	en	Nigger Cay						
1455550	3576959		Mouchoir Carré Passage						
1455551	3576959		Passe du Mouchoir carre						
1455552	3576959		Passe du Mouchoir Carre						
4314622	3576959		Mouchoir Passage						
1455553	3576961		Molasses Reefs						
4314623	3576961		Molasses Reef						
11912746	3576962	en	Middleton Cay						
11912747	3576963	en	Middle Creek Cay						
1455554	3576964		Grand Caicos Island						
1455555	3576964		Grande Caïque						
1455556	3576964		Grand Caicos						
4314624	3576964		Middle Caicos						
6889408	3576964	link	https://en.wikipedia.org/wiki/Middle_Caicos						
12252105	3576964	iata	MDS						
13344353	3576964	ko	미들 카이코스						
13800107	3576964	unlc	TCMDS						
11912748	3576967	en	Major Hill Cay						
1627199	3576968	ca	Lorimers						
4314625	3576968		Lorimers						
11912749	3576969	en	Lorimer Creek						
1455557	3576971		Pelican Cay						
1455558	3576971		Ile Longue						
4314626	3576971		Long Cay						
11912750	3576972	en	Long Bay						
11912751	3576979	en	Kew						
11912752	3576980	en	Juniper Hole						
11912753	3576981	en	Joe Grants Cay						
11912754	3576982	en	Jacksonville Cut						
11912755	3576984	en	Iguana Cay						
11912756	3576985	en	Hog Cay						
11912757	3576987	en	Highas Cay						
11912758	3576988	en	Hawks Nest Anchorage						
11912759	3576989	en	Haulover Point						
15633899	3576989	wkdt	Q34921663						
11912760	3576990	en	Gun Hill						
11912761	3576991	en	Greenwich						
11912762	3576992	en	Grassy Creek						
2986174	3576993	link	https://en.wikipedia.org/wiki/Grand_Turk_Island						
4314627	3576993		Grand Turk Island						
8103495	3576993	iata	GDT						
11598101	3576993	es	Isla Gran Turca						
11598102	3576993	gl	Gran Turca						
11598103	3576993	lt	Grand Terkas						
11598104	3576993	ru	Гранд-Терк						
11598105	3576993	zh	大特克島						
13344355	3576993	ko	그랜드 터크						
13800106	3576993	unlc	TCGDT						
1455560	3576994	en	Cockburn Town						
1649755	3576994	es	Cockburn Town						
1649829	3576994	pt	Cockburn Town						
5424020	3576994	link	https://en.wikipedia.org/wiki/Cockburn_Town						
5463700	3576994	no	Cockburn Town						
5463701	3576994	da	Cockburn Town						
5463702	3576994	sv	Cockburn Town						
5463703	3576994	de	Cockburn Town						
9449105	3576994	az	Kobərn Taun						
9449106	3576994	be	Коберн-Таўн						
9449107	3576994	bg	Кокбърн Таун						
9449108	3576994	cv	Коберн-Таун						
9449109	3576994	el	Κόκμπερν Τάουν						
9449110	3576994	eo	Kokburno						
9449111	3576994	fa	کوک برن تاون، باهاما						
9449112	3576994	he	קוקבורן טאון						
9449113	3576994	ja	コックバーンタウン						
9449114	3576994	ka	კოკბერნ-ტაუნი						
9449115	3576994	ko	코번타운						
9449116	3576994	lt	Kobern Taunas						
9449117	3576994	mr	कॉकबर्न टाउन						
9449118	3576994	sr	Коберн Таун						
9449119	3576994	th	ค็อกเบิร์นทาวน์						
9449120	3576994	uk	Коберн-Таун						
9449121	3576994	ur	کاک برن ٹاؤن						
9449122	3576994	ru	Коберн-Таун						
9449123	3576994	zh	科伯恩城						
11459329	3576994	mk	Коберн Таун						
11459330	3576994	ta	காக்பேர்ண் நகரம்						
15765462	3576994	wkdt	Q34205						
1455561	3576997		Gibb Cay						
4314628	3576997		Gibb’s Cay						
16812823	3577000		Fort George Cay						
16812824	3577000		Fort George						
11912763	3577002	en	Flamingo Pond						
11912764	3577003	en	Flamingo Hill						
11912765	3577007	en	Ferguson Cut						
11912766	3577008	en	Farm Creek Pond						
1455562	3577009		Endymion Shoal						
4314629	3577009		Endymion Rock						
1455563	3577011		Breeches Island						
4314630	3577011		East Cay						
1455564	3577012	fr	Caïque de l'Est						
1627198	3577012	ca	Illes Caicos de l'Est						
4314631	3577012		East Caicos Island						
7520494	3577012	link	https://en.wikipedia.org/wiki/East_Caicos						
16812825	3577012	en	East Caicos						
11912767	3577013	en	East Bay						
11912768	3577014	en	East Bay						
1455565	3577015		Cape Comete						
1627197	3577015	ca	Cap Drum						
4314632	3577015		Drum Point						
15548164	3577015	wkdt	Q34853829						
11806859	3577018		Dickish Cay						
11806860	3577018		Dikish Cay						
6890075	3577019	link	https://en.wikipedia.org/wiki/Dellis_Cay						
6923173	3577019		Dellis Cay						
11912769	3577020	en	The Creeks						
1455566	3577021		Caye a Coton						
4314633	3577021		Cotton Cay						
11912770	3577023	en	Conch Cay						
8675882	3577024	link	https://en.wikipedia.org/wiki/Conch_Bar_Caves						
8802875	3577024		Conch Bar Caves						
11912771	3577025	en	Conch Bar						
15269362	3577026	wkdt	Q34841751						
15959512	3577026		Company Point						
7040977	3577027	link	https://en.wikipedia.org/wiki/Cockburn_Harbour						
7119015	3577027		Cockburn Harbour						
13800105	3577027	unlc	TC9CB						
1455567	3577028		East Harbour						
4314634	3577028		Cockburn Harbour						
11806861	3577029		Clearsand Roads						
11806862	3577029		Clearsand Road						
11912772	3577030	en	Clarke’s Cut						
1455569	3577034		Caicos						
1455570	3577034		Les Caïques						
4314636	3577034		Caicos Islands						
11912773	3577036	en	Caicos Bank						
1627196	3577038	ca	Cap Breezy						
4314637	3577038		Breezy Point						
15267383	3577038	wkdt	Q31925977						
11806863	3577039		Bottle Creek Settlements						
11806864	3577039		Bottle Creek						
11912774	3577040	en	Bottle Creek Mouth						
1455571	3577041		Bottlenose Creek						
4314638	3577041		Bottle Creek						
11912775	3577042	en	Booby Island						
15529337	3577042	wkdt	Q31924343						
1455572	3577043		Blue Hills						
4314639	3577043		Blue Hills Settlements						
1455573	3577044		Sand Cay						
4314640	3577044		Big Sand Cay						
11912776	3577045	en	Big Pond						
11912777	3577047	en	Big Cut						
11912778	3577048	en	Big Cameron Cay						
15499835	3577048	wkdt	Q31915879						
7501164	3577049	link	https://en.wikipedia.org/wiki/Ambergris_Cay						
7566393	3577049		Big Ambergris Cay						
11912779	3577052	en	Bay Cay						
15575995	3577052	wkdt	Q31912498						
11912780	3577053	en	Bambarra						
11912781	3577056	en	Armstrong Pond						
1455574	3577057		Cayes d’Ambre gris						
4314641	3577057		Ambergris Cays						
11915246	3740265	en	Grassy Creek						
1886443	6299791	icao	MBPV						
1889206	6299791	iata	PLS						
5805855	6299791	link	https://en.wikipedia.org/wiki/Providenciales_International_Airport						
8522317	6299791	en	Providenciales International Airport						
8522318	6299791	yue	普羅維登西亞萊斯國際機場						
8522319	6299791	fa	فرودگاه بین‌المللی پراویدنشیالز						
9709045	6299791	de	Flughafen Providenciales						
9709046	6299791	fr	Aéroport International de Providenciales						
15232549	6299791	wkdt	Q1748436						
5808504	7668339	icao	MBGT						
5809249	7668339	iata	GDT						
5809994	7668339	link	https://en.wikipedia.org/wiki/JAGS_McCartney_International_Airport						
8567828	7668339	en	Grand Turk International Airport						
8567831	7668339	en	JAGS McCartney International Airport						
15234258	7668339	wkdt	Q2902808						
5808052	7668340	icao	MBNC						
5808797	7668340	iata	NCA						
5809542	7668340	link	https://en.wikipedia.org/wiki/North_Caicos_Airport						
15740301	7668340	wkdt	Q7054343						
5808053	7668341	icao	MBSC						
5808798	7668341	iata	XSC						
5809543	7668341	link	https://en.wikipedia.org/wiki/South_Caicos_Airport						
5894419	7668341	pl	Port lotniczy South Caicos						
15321579	7668341	wkdt	Q7566471						
5885402	7730647	iata	MDS						
5885403	7730647	icao	MBMC						
6895446	7730647	link	https://en.wikipedia.org/wiki/Middle_Caicos_Airport						
15688159	7730647	wkdt	Q3566509						
5885404	7730648	iata	PIC						
5885405	7730648	icao	MBPI						
5885406	7730649	iata	SLX						
5885407	7730649	icao	MBSY						
7511276	7730649	link	https://en.wikipedia.org/wiki/Salt_Cay_Airport						
8514712	7730649	en	Salt Cay Airport						
8514713	7730649	fr	Aéroport de Salt Cay						
16184823	11201380	link	https://en.wikipedia.org/wiki/Wheeland						
16225532	11201380		Wheeland						
15661378	11201604	wkdt	Q34842196						
16071679	11201604		Cooper Jack Point						
15618998	11201613	wkdt	Q34951262						
16071680	11201613		Juba Point						
15508064	11201614	wkdt	Q31926124						
16071681	11201614		Bristol Hill Point						
15647616	11201616	wkdt	Q34863974						
16071682	11201616		Extreme Point						
15242702	11201628	wkdt	Q34844000						
16071683	11201628		Crisis Point						
11922793	11201649	en	Wade’s Green Plantation						
11922794	11201654	en	St Thomas Hill						
11922795	11201659	en	Moore Hall Pond						
11922796	11201660	en	Teren Hill Plantation						
11922797	11201661	en	Teren Hill						
11922798	11201663	en	Pumpkin Bluff						
11922799	11201664	en	Whitby Salina						
11922800	11201665	en	Hollywood Hill						
11922801	11201666	en	Snaky Pond						
11922802	11201667	en	Whitby Cove						
11922803	11201668	en	Mally Pond						
11922804	11201765	en	Whitby Bight						
11922805	11201801	en	Red Salina						
11922806	11201833	en	Pelican Point						
11922807	11201834	en	Greenwich Hill						
11922808	11201835	en	Tattiland Down Pond						
11922809	11201836	en	Leeward Mouth						
11922810	11201837	en	Greenwich Creek						
11922811	11201838	en	North Mouth						
11922812	11201839	en	Bernald’s Point						
11922813	11201840	en	Sand Bar Point						
11922814	11201841	en	Spanish Point						
11922815	11201842	en	Hungry Hole						
11922816	11201843	en	East Bay Beach						
11922817	11201844	en	Major Hill						
11922818	11201845	en	Smith						
11922819	11201846	en	Israel Field						
11922820	11201847	en	Burial Cay						
11922821	11201848	en	Belleview Wharf						
11922822	11201849	en	Belleview						
11922823	11201850	en	Mountain Pond						
11922824	11201851	en	Taylor’s Wharf						
11922825	11201852	en	Belvedere						
11922826	11201853	en	North Windsor						
11922827	11201854	en	William’s Wharf						
11922828	11201855	en	South Windsor						
11922829	11201856	en	Government Wharf						
11922830	11201857	en	Karmont						
11922831	11201858	en	West Well						
11922832	11201859	en	Richmond						
11922833	11201860	en	Laughland						
11922834	11201861	en	Belmont						
11922835	11201862	en	First Cay						
11922836	11201863	en	Crossing Place Channel						
11922837	11201864	en	Robin Channel						
11922838	11201865	en	South Mouth						
11922839	11201866	en	Rat Cay						
11922840	11201867	en	Dane Cay Point						
15249418	11201867	wkdt	Q34845901						
11922841	11201872	en	Rocky Hill						
11922842	11201873	en	Rocky Hill Ponds						
11922843	11201874	en	Sandy Hill						
11922844	11201875	en	Fish Ponds						
11922845	11201876	en	Norbellis Coves						
11922846	11201877	en	Mudjin Harbour Point						
11922847	11201878	en	King Hill						
11922848	11201879	en	Big Hill						
11922849	11201880	en	Harrison Hill						
11922850	11201881	en	Benjie Landing						
11922851	11201882	en	Stubb’s Landing						
11922852	11201883	en	Village Hill						
11922853	11201884	en	Button Wood Pond						
11922854	11201885	en	Village Pond						
11922855	11201886	en	Jack Pond						
11922856	11201887	en	English Pond						
11922857	11201888	en	Trevellyan Point						
11922858	11201889	en	Kitty Pond						
11922859	11201890	en	Platico Point						
11922860	11201891	en	Nongatown Landing						
11922861	11201892	en	Nongatown						
11922862	11201893	en	Ferguson Plantation						
11922863	11201894	en	Stubb’s Plantation						
11922864	11201895	en	Flamingo Pond						
11922865	11201896	en	Montpeller Hill						
11922866	11201897	en	Montpeller Pond						
11922867	11201898	en	Corry Pond						
11922868	11201899	en	Duck Pond						
11922869	11201900	en	Vine Point						
11922870	11201901	en	The Bambarra Landings						
11922871	11201902	en	Pelican Cay						
11922872	11201903	en	Inner Cay						
11922873	11201904	en	Outer Cay						
11922874	11201905	en	Gambol Point						
15624566	11201905	wkdt	Q34893526						
11922875	11201906	en	Farm Creek Salina						
11922876	11201907	en	Big Landing						
11922877	11201914	en	Flamingo Pond						
11922878	11201915	en	Black Rock Salina						
11922879	11201916	en	Thatch Cay						
11922880	11201917	en	Rocky Point						
11922881	11201918	en	Drum Point Bay						
11922882	11201920	en	White Salina Bank						
11922883	11201922	en	Cow Hill Point						
15699274	11201922	wkdt	Q34843087						
11922884	11201923	en	West Harbour Creek Point						
11922885	11201924	en	East Harbour Creek Point						
15685479	11201924	wkdt	Q34856679						
11922886	11201925	en	Tony Rock						
11922887	11201926	en	Haulover Fields						
11922888	11201927	en	The Creek						
11922889	11201928	en	Finaway Creek Point						
15867201	11201928	wkdt	Q34866796						
11922890	11201929	en	Jack Pond						
11922891	11201930	en	The Point						
11922892	11201931	en	Joseph Cays						
11922893	11201932	en	Joseph Cay						
11922894	11201933	en	Tucker Point						
11922895	11201934	en	Increase Bay						
11922896	11201935	en	Increase Point						
15351966	11201935	wkdt	Q34942741						
11922897	11201936	en	Bina Landing Swamp						
11922898	11201937	en	Increase Hill						
11922899	11201938	en	Eel Pond						
11922900	11201939	en	Increase Plantation						
11922901	11201940	en	Jacksonville						
11922902	11201941	en	Lorimers Point						
11922903	11201942	en	Jackson Cut Bay						
11922904	11201943	en	Joe Grant’s Cay Point						
11922905	11201944	en	Long Bay Beach						
11922906	11201945	en	Cedar Point						
11922907	11201946	en	Middle Of Channel Cay						
11922908	11201947	en	Tenny Scot Cut						
11922909	11201948	en	Tawka Point						
11922910	11201949	en	Toll Crawl Point						
11922911	11201950	en	Cameron Cay Mangrove						
11922912	11202286	en	Fowl Cay						
11922913	11202287	en	Table Cay						
11922914	11202288	en	Middle Cay Cut						
11922915	11202289	en	Plandon Cay Cut						
11922916	11202290	en	Big Cay						
11922917	11202291	en	Sand Bore Cay						
11922918	11202292	en	Loran Station						
11922919	11202293	en	Nuisance Point						
11922920	11202294	en	Sail Rock Estate						
11922921	11202295	en	Bonefish Hole						
11922922	11202296	en	Long Beach						
11922923	11202297	en	Iguana Cay						
11922924	11202298	en	Highlands Cut						
11922925	11202299	en	Valley Bay						
11922926	11202300	en	Two Sister’s Hill						
11922927	11202301	en	Spirit Point						
11922928	11202302	en	Bell Sound						
11922929	11202303	en	Horse Cay						
11922930	11202304	en	Stubb’s West Canal						
11922931	11202305	en	Butterfield Well						
11922932	11202306	en	Goat Hill						
11922933	11202307	en	Duck Pond Plantations						
11922934	11202308	en	Lightbourne’s Well						
11922935	11202309	en	Victoria Salina						
11922936	11202310	en	Durham’s West Field						
11922937	11202311	en	Sarah’s Hill						
11922938	11202312	en	Stubb’s East Canal						
11922939	11202313	en	Tatem Field Hill						
11922940	11202314	en	Robert Todd Bay						
11922941	11202315	en	Highlands Bay						
11922942	11202316	en	High Point						
11922943	11202317	en	Highlands Estate						
11922944	11202318	en	Shark Bay						
11922945	11202319	en	Godet Field						
11922946	11202320	en	Mooney Point						
11922947	11202321	en	Basden’s Hill						
11922948	11202322	en	The Basden Ponds						
11922949	11202323	en	Basden’s Well						
11922950	11202324	en	Fountain Well						
11922951	11202325	en	Spencer’s Landing						
11922952	11202326	en	Dillon Point						
11922953	11202327	en	Parson’s Point						
11922954	11202328	en	Buoy Bay						
11922955	11202329	en	Rocky Wharf						
11922956	11202333	en	Bermudian Harbour						
11922957	11202334	en	Big Cut						
11922958	11202335	en	Dove Cay						
11922959	11202336	en	Girl’s Bay						
11922960	11202337	en	Upper Cut						
11922961	11202338	en	Annie Lewis Cut						
11922962	11202339	en	Before-Day Cut						
11922963	11202340	en	Fish Rock						
11922964	11202374	en	Aker’s Hill						
11922965	11202375	en	Mastersons Point						
11922966	11202378	en	South Wells						
11922967	11202379	en	Mount Windom						
11922968	11202380	en	White Sands Beach						
15272026	11202381	wkdt	Q31924346						
16071684	11202381		Booby Rock Point						
11922969	11202382	en	South Creek						
11922970	11202383	en	Round Cay						
11922971	11202384	en	Small Cut						
11922972	11202386	en	Talbot Shoal						
11922973	11202387	en	Little Sand Cay						
11922974	11202388	en	North East Point						
11922975	11202390	en	Whale Island						
11922976	11202391	en	Whale Island Bay						
11922977	11202391	en	Whale House Bay						
11922978	11202392	en	Long Bay Point						
11922979	11202394	en	Long Bay						
11922980	11202398	en	Taylor’s Hill						
11922981	11202399	en	The North Creek						
11922982	11202400	en	The South Creek						
11922983	11202402	en	Prickly Point						
11922984	11202404	en	South Point						
11922985	11202405	en	Pilchard Hole						
11941218	11250530	en	Gerry Yard						
11941307	11250595	en	The Turnup						
11941308	11250596	en	Stubb’s Crawl						
11941309	11250597	en	Conch Ground						
11941310	11250598	en	The Flat						
11941311	11250599	en	Highland House						
11941312	11250600	en	Lorimers Landing						
11941313	11250601	en	Malcom’s Well						
11941314	11250602	en	Cape Comete Hill						
11941526	11250732	en	Salt Pond						
11941527	11250733	en	Fish-hawks Mark						
11941528	11250734	en	Tucker Point						
11941529	11250735	en	The Spout						
11941530	11250736	en	The Pillory						
11941531	11250737	en	Palm Grove						
11941532	11250738	en	The Sound						
11941533	11250739	en	Fire Hill						
11941534	11250740	en	Quadga Bay						
11941535	11250741	en	Grey Salina						
11941536	11250742	en	Dunscom Point						
15288233	11250742	wkdt	Q34854532						
11941537	11250743	en	Middle Rock						
11941538	11250744	en	South Rock						
11941539	11250745	en	Nor’ West Point						
11941540	11250746	en	Beacon Hill						
11941706	11251175	en	Three Marys						
11941707	11251175	en	South Rocks						
15514380	11820579	wkdt	Q13525311						
16151663	11820579	link	https://en.wikipedia.org/wiki/Grand_Turk_Lighthouse						
