1226304	2658099		Grauspitz						
5713033	2658099	link	https://en.wikipedia.org/wiki/Grauspitz						
1227057	2660346		Grauspitz						
3075695	2660346		Schwarzhorn		1				
5712704	2660346	link	https://en.wikipedia.org/wiki/Hinter_Grauspitz						
1298324	3042029		Vorderer Schellenberg						
1298325	3042029		Vorder-Schellenberg						
4332839	3042029		Vorderschellenberg						
1596298	3042030	de	Vaduz						
1596299	3042030	en	Vaduz						
1596300	3042030	es	Vaduz						
1596301	3042030	als	Vaduz						
1596302	3042030	bg	Вадуц						
1596303	3042030	ca	Vaduz						
1596304	3042030	cs	Vaduz						
1596305	3042030	da	Vaduz						
1596306	3042030	el	Βαντούζ						
1596307	3042030	eo	Vaduzo						
1596308	3042030	et	Vaduz						
1596309	3042030	fi	Vaduz						
1596310	3042030	fr	Vaduz						
1596311	3042030	he	פאדוץ						
1596312	3042030	hr	Vaduz						
1596313	3042030	id	Vaduz						
1596314	3042030	io	Vaduz						
1596315	3042030	is	Vaduz						
1596316	3042030	it	Vaduz						
1596317	3042030	ja	ファドゥーツ						
1596318	3042030	ko	파두츠						
1596319	3042030	nl	Vaduz						
1596320	3042030	no	Vaduz						
1596321	3042030	pl	Vaduz						
1596322	3042030	pt	Vaduz						
1596323	3042030	ro	Vaduz						
1596324	3042030	ru	Вадуц						
1596325	3042030	sk	Vaduz						
1596326	3042030	sl	Vaduz						
1596327	3042030	sr	Вадуц						
1596328	3042030	sv	Vaduz						
1596329	3042030	zh	瓦都茲						
1620509	3042030	be	Вадуц						
1620510	3042030	ka	ვადუცი						
1620511	3042030	lt	Vaducas						
1620512	3042030	tr	Vaduz						
1895855	3042030	bs	Vaduz						
1895856	3042030	eu	Vaduz						
1895857	3042030	hu	Vaduz						
1895858	3042030	oc	Vaduz						
1895859	3042030	scn	Vaduz						
1895860	3042030	sq	Vaduz						
1895861	3042030	vo	Vaduz						
1981814	3042030	am	ፋዱጽ						
1981815	3042030	frp	Vaduz						
1981816	3042030	qu	Vaduz						
1981817	3042030	sw	Vaduz						
2968601	3042030	link	https://en.wikipedia.org/wiki/Vaduz						
3055048	3042030	link	https://ru.wikipedia.org/wiki/%D0%92%D0%B0%D0%B4%D1%83%D1%86						
7485594	3042030	iata	QVU						
8103396	3042030	post	9490						
13786231	3042030	unlc	LIVDZ						
15286627	3042030	wkdt	Q27439086						
1298326	3042031		Vaduz		1				
5650172	3042031		Gemeinde Vaduz						
8234450	3042031	link	https://en.wikipedia.org/wiki/Vaduz						
15325488	3042031	wkdt	Q1844						
10846760	3042032		Under Riet						
10846761	3042032		Unteres Riet						
1907641	3042033	de	Triesenberg						
1907642	3042033	es	Triesenberg						
1907643	3042033	fr	Triesenberg						
1907644	3042033	pl	Triesenberg						
1907645	3042033	en	Triesenberg						
1907646	3042033	eo	Triesenberg						
1907647	3042033	fi	Triesenberg						
1907648	3042033	it	Triesenberg						
1907649	3042033	ja	トリーゼンベルク						
1907650	3042033	nl	Triesenberg						
1907651	3042033	pt	Triesenberg						
1995210	3042033	ca	Triesenberg						
1995211	3042033	eu	Triesenberg						
3055049	3042033	link	https://ru.wikipedia.org/wiki/%D0%A2%D1%80%D0%B8%D0%B7%D0%B5%D0%BD%D0%B1%D0%B5%D1%80%D0%B3						
8103403	3042033	post	9497						
1298327	3042034		Triesenberg / Steg / Malbun						
1298328	3042034		Triesenberg		1				
5650173	3042034		Gemeinde Triesenberg						
8234449	3042034	link	https://en.wikipedia.org/wiki/Triesenberg						
15329580	3042034	wkdt	Q49651						
1909842	3042035	de	Triesen						
1909843	3042035	es	Triesen						
1909844	3042035	fr	Triesen						
1909845	3042035	pl	Triesen						
1909846	3042035	en	Triesen						
1909847	3042035	eo	Triesen						
1909848	3042035	fi	Triesen						
1909849	3042035	it	Triesen						
1909850	3042035	ja	トリーゼン						
1909851	3042035	nl	Triesen						
1996954	3042035	ca	Triesen						
1996955	3042035	eu	Triesen						
2968603	3042035	link	https://en.wikipedia.org/wiki/Triesen						
3055050	3042035	link	https://ru.wikipedia.org/wiki/%D0%A2%D1%80%D0%B8%D0%B7%D0%B5%D0%BD						
4332840	3042035		Triesen						
8103401	3042035	post	9495						
8486923	3042035	ru	Тризен						
13786230	3042035	unlc	LITES						
1298329	3042036		Triesen		1				
5650174	3042036		Gemeinde Triesen						
8234448	3042036	link	https://en.wikipedia.org/wiki/Triesen						
14855647	3042036	wkdt	Q49654						
1907629	3042037	es	Schellenberg						
1907630	3042037	de	Schellenberg						
1907631	3042037	fr	Schellenberg						
1907632	3042037	pl	Schellenberg						
1907633	3042037	en	Schellenberg						
1907634	3042037	eo	Schellenberg						
1907635	3042037	fi	Schellenberg						
1907636	3042037	it	Schellenberg						
1907637	3042037	ja	シェレンベルク						
1907638	3042037	nl	Schellenberg						
1907639	3042037	no	Schellenberg						
1907640	3042037	pt	Schellenberg						
1995194	3042037	ca	Schellenberg						
1995195	3042037	eu	Schellenberg						
3058080	3042037	link	https://ru.wikipedia.org/wiki/%D0%A8%D0%B5%D0%BB%D0%BB%D0%B5%D0%BD%D0%B1%D0%B5%D1%80%D0%B3_%28%D0%9B%D0%B8%D1%85%D1%82%D0%B5%D0%BD%D1%88%D1%82%D0%B5%D0%B9%D0%BD%29						
8103395	3042037	post	9488						
1298330	3042038		Schellenberg		1				
5650175	3042038		Gemeinde Schellenberg						
8234447	3042038	link	https://en.wikipedia.org/wiki/Schellenberg						
15206817	3042038	wkdt	Q49655						
1298331	3042039		Scheuenkopf						
1298332	3042039		Matlerkopf	1					
3075666	3042039		Scheienkopf						
7069880	3042040	link	https://en.wikipedia.org/wiki/Schaanwald						
8103393	3042040	post	9486						
1298333	3042041		Schan						
1905682	3042041	de	Schaan						
1905683	3042041	es	Schaan						
1905684	3042041	fr	Schaan						
1905685	3042041	pl	Schaan						
1905686	3042041	en	Schaan						
1905687	3042041	eo	Schaan						
1905688	3042041	fa	شان						
1905689	3042041	fi	Schaan						
1905690	3042041	it	Schaan						
1905691	3042041	ja	シャーン						
1905692	3042041	nl	Schaan						
1905693	3042041	pt	Schaan						
1905694	3042041	sv	Schaan						
1991933	3042041	ca	Schaan						
1991934	3042041	da	Schaan						
1991935	3042041	eu	Schaan						
3055051	3042041	link	https://ru.wikipedia.org/wiki/%D0%A8%D0%B0%D0%BD_%28%D0%9B%D0%B8%D1%85%D1%82%D0%B5%D0%BD%D1%88%D1%82%D0%B5%D0%B9%D0%BD%29						
8103400	3042041	post	9494						
8486924	3042041	ru	Шаан						
13786229	3042041	unlc	LISCN						
1298334	3042042		Schaan		1				
5650176	3042042		Gemeinde Schaan						
8234446	3042042	link	https://en.wikipedia.org/wiki/Schaan						
14937206	3042042	wkdt	Q49657						
10846762	3042043		Sareiserjoch						
10846763	3042043		Sareiser Joch						
1298335	3042044		Samina Thal						
1298336	3042046		Rugell						
1918096	3042046	de	Ruggell						
1918097	3042046	es	Ruggell						
1918098	3042046	fr	Ruggell						
1918099	3042046	pl	Ruggell						
1918100	3042046	en	Ruggell						
1918101	3042046	eo	Ruggell						
1918102	3042046	it	Ruggell						
1918103	3042046	ja	ルッゲル						
1918104	3042046	nl	Ruggell						
2000612	3042046	ca	Ruggell						
2000613	3042046	eu	Ruggell						
3042374	3042046	ru	Руггелль						
3055052	3042046	link	https://ru.wikipedia.org/wiki/%D0%A0%D1%83%D0%B3%D0%B3%D0%B5%D0%BB%D0%BB%D1%8C						
4332841	3042046		Ruggell						
8103397	3042046	post	9491						
13786227	3042046	unlc	LIRGL						
16241593	3042046	link	https://en.wikipedia.org/wiki/Ruggell						
1298337	3042047		Ruggell		1				
5650177	3042047		Gemeinde Ruggell						
8234445	3042047	link	https://en.wikipedia.org/wiki/Ruggell						
14937418	3042047	wkdt	Q49659						
1298338	3042048		Rhätikon Mountains						
1298339	3042048		Rhaetikon						
1298340	3042048	de	Rätikon						
1298341	3042049		Blanken						
1907771	3042049	de	Planken						
1907772	3042049	es	Planken						
1907773	3042049	fr	Planken						
1907774	3042049	pl	Planken						
1907775	3042049	en	Planken						
1907776	3042049	eo	Planken						
1907777	3042049	fi	Planken						
1907778	3042049	it	Planken						
1907779	3042049	ja	プランケン						
1907780	3042049	nl	Planken						
1907781	3042049	pt	Planken						
1995173	3042049	ca	Planken						
1995174	3042049	eu	Planken						
3042375	3042049	ru	Планкен						
3055053	3042049	link	https://ru.wikipedia.org/wiki/%D0%9F%D0%BB%D0%B0%D0%BD%D0%BA%D0%B5%D0%BD						
8103404	3042049	post	9498						
13786226	3042049	unlc	LIPLK						
1298342	3042050		Planken		1				
5650178	3042050		Gemeinde Planken						
8234566	3042050	link	https://en.wikipedia.org/wiki/Planken						
14910063	3042050	wkdt	Q49660						
1298343	3042051		Ochsenberg						
5712756	3042051	link	https://en.wikipedia.org/wiki/Ochsenkopf_%28Malbun%29						
7068913	3042052	link	https://en.wikipedia.org/wiki/Nendeln						
8103392	3042052	post	9485						
15660534	3042052	wkdt	Q991615						
1298345	3042054		Mittler-Schellenberg						
1298346	3042054		Mittlerer Schellenberg						
4332842	3042054		Mittlerschellenberg						
1298347	3042055		Mauern						
1917990	3042055	de	Mauren						
1917991	3042055	es	Mauren						
1917992	3042055	fr	Mauren						
1917993	3042055	pl	Mauren						
1917994	3042055	en	Mauren						
1917995	3042055	eo	Mauren						
1917996	3042055	it	Mauren						
1917997	3042055	ja	マウレン						
1917998	3042055	nl	Mauren						
2000609	3042055	ca	Mauren						
2000610	3042055	eu	Mauren						
3058081	3042055	link	https://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D1%83%D1%80%D0%B5%D0%BD_%28%D0%9B%D0%B8%D1%85%D1%82%D0%B5%D0%BD%D1%88%D1%82%D0%B5%D0%B9%D0%BD%29						
8103399	3042055	post	9493						
13521617	3042055		Mauren FL						
13786225	3042055	unlc	LIMAU						
1298348	3042056		Mauren		1				
1298349	3042056		Mauren-Schaanwald						
5650179	3042056		Gemeinde Mauren						
8234444	3042056	link	https://en.wikipedia.org/wiki/Mauren						
11503400	3042056	be	Маўрэн						
11503401	3042056	fa	ماورن						
11503402	3042056	he	מאורן						
11503403	3042056	ja	マウレン						
11503404	3042056	ko	마우렌						
11503405	3042056	lt	Maurenas						
11503406	3042056	mk	Маурен						
11503407	3042056	pnb	ماؤرن						
11503408	3042056	th	เมาเริน						
11503409	3042056	uk	Маурен						
11503410	3042056	ur	ماویرن						
11503411	3042056	ru	Маурен						
11503412	3042056	zh	毛伦						
14869852	3042056	wkdt	Q49661						
1298350	3042057		Matlerjoch						
1298351	3042057		Matla Joch						
15775764	3042057	wkdt	Q27439078						
1298352	3042058	de	Fürstentum Liechtenstein						
1298354	3042058		Lichtenstein						
1562330	3042058	am	ሊችተንስታይን	1					
1562331	3042058	ar	ليختنشتاين	1					
1562332	3042058	bg	Лихтенщайн	1					
1562333	3042058	ca	Liechtenstein	1					
1562334	3042058	cs	Lichtenštejnsko	1					
1562335	3042058	cy	Liechtenstein	1					
1562336	3042058	da	Liechtenstein	1					
1562337	3042058	de	Liechtenstein	1					
1562338	3042058	el	Λιχτενστάιν	1					
1562339	3042058	en	Liechtenstein	1					
1562340	3042058	eo	Liĥtenŝtejno	1					
1562341	3042058	es	Liechtenstein	1					
1562342	3042058	et	Liechtenstein	1					
1562343	3042058	fa	لیختن‌اشتاین	1					
1562344	3042058	fi	Liechtenstein	1					
1562345	3042058	fo	Liktinstein	1					
1562346	3042058	fr	Liechtenstein	1					
1562347	3042058	ga	Lichtinstéin	1					
1562348	3042058	he	ליכטנשטיין	1					
1562349	3042058	hi	लिकटेंस्टीन						
1562350	3042058	hr	Lihtenštajn	1					
1562351	3042058	hu	Liechtenstein	1					
1562352	3042058	hy	Լիխտենշտեյն	1					
1562353	3042058	id	Liechtenstein	1					
1562354	3042058	is	Liechtenstein	1					
1562355	3042058	it	Liechtenstein	1					
1562356	3042058	ja	リヒテンシュタイン	1					
1562357	3042058	ka	ლიხტენშტეინი						
1562358	3042058	km	លិចតិនស្ដាញ	1					
1562359	3042058	ko	리히텐슈타인	1					
1562360	3042058	lo	ລິດເທນສະຕາຍ	1					
1562361	3042058	lt	Lichtenšteinas	1					
1562362	3042058	lv	Lihtenšteina	1					
1562363	3042058	mk	Лихтенштајн	1					
1562364	3042058	ms	Liechtenstein	1					
1562365	3042058	mt	il-Liechtenstein	1					
1562366	3042058	nb	Liechtenstein	1					
1562367	3042058	nl	Liechtenstein	1					
1562368	3042058	nn	Liechtenstein						
1562369	3042058	pl	Liechtenstein	1					
1562370	3042058	pt	Liechtenstein	1					
1562371	3042058	ro	Liechtenstein	1					
1562372	3042058	ru	Лихтенштейн	1					
1562373	3042058	sk	Lichtenštajnsko	1					
1562374	3042058	sl	Liechtenstein						
1562375	3042058	sq	Lihtenshtajn	1					
1562376	3042058	sr	Лихтенштајн	1					
1562377	3042058	sv	Liechtenstein	1					
1562378	3042058	ta	லிச்செண்ஸ்டெய்ன்	1					
1562379	3042058	th	ลิกเตนสไตน์	1					
1562380	3042058	tr	Liechtenstein	1					
1562381	3042058	uk	Ліхтенштейн	1					
1562382	3042058	vi	Lich-ten-xtên						
1562383	3042058	zh	列支敦士登	1					
1618676	3042058	af	Liechtenstein	1					
1618677	3042058	als	Liechtenstein						
1618678	3042058	an	Liechtenstein						
1618679	3042058	ast	Liechtenstein						
1618680	3042058	be	Ліхтэнштайн						
1618681	3042058	br	Liechtenstein	1					
1618682	3042058	eu	Liechtenstein	1					
1618683	3042058	fy	Lychtenstein						
1618684	3042058	gl	Liechtenstein	1					
1618685	3042058	ht	Lichtènstayn						
1618686	3042058	io	Liechtenstein						
1618687	3042058	kw	Liechtenstein						
1618688	3042058	la	Lichtenstenum						
1618689	3042058	li	Liechtenstein						
1618690	3042058	nds	Liechtensteen						
1618691	3042058	no	Liechtenstein	1					
1618692	3042058	oc	Categoria:Liechtenstein						
1618693	3042058	oc	Liechtenstein						
1618694	3042058	rm	Liechtenstein	1					
1618695	3042058	scn	Liechtenstein						
1618696	3042058	sl	Lihtenštajn	1					
1618697	3042058	sq	Lihtenshtajni						
1618698	3042058	th	ประเทศลิกเตนสไตน์						
1618699	3042058	tl	Liechtenstein						
1618700	3042058	za	Luliilu						
1618701	3042058	zh	列支敦斯登						
1632754	3042058	fa	لیختنشتاین						
1632755	3042058	ug	لىختېنشتېين						
1894078	3042058	arc	ܠܝܟܛܢܫܛܝܢ						
1894079	3042058	bar	Liechtnstâ						
1894080	3042058	eu	Liechtestein						
1894081	3042058	ka	ლიხტენშტაინი	1					
1894082	3042058	ku	Liechtenstein	1					
1894083	3042058	ku	Lîştinştayn						
1894084	3042058	lb	Liechtenstein	1					
1894085	3042058	ne	लीश्टेनस्टाईन						
1894086	3042058	pam	Liechtenstein						
1894087	3042058	qu	Liechtenstein	1					
1894088	3042058	tet	Listenstaina						
1894089	3042058	uz	Lixtenshteyn	1					
1894090	3042058	vo	Ligtänstän						
1975853	3042058	bar	Liachtnstoa						
1975854	3042058	bs	Lihtenštajn	1					
1975855	3042058	frp	Liechtenstein						
1975856	3042058	hi	लिख्टेंश्टाइन						
1975857	3042058	hsb	Liechtenstein						
1975858	3042058	lij	Liechtenstein						
1975859	3042058	pms	Lichtenstàin						
1975860	3042058	hbs	Lihtenštajn						
1975861	3042058	tr	Lihtenştayn						
1975862	3042058	vi	Liechtenstein	1					
1975863	3042058	war	Lestenstin						
2421559	3042058	be	Ліхтэнштэйн	1					
2421560	3042058	bo	ལེག་ཏེན་ཚིན།	1					
2421561	3042058	fa	لیکتنستین	1					
2421562	3042058	hi	लियक़्टँस्टीन	1					
2421563	3042058	ia	Liechtenstein	1					
2421564	3042058	it	Principato del Liechtenstein						
2421565	3042058	ja	リヒテンシュタイン公国	1					
2421566	3042058	pl	Lichtenstein	1					
2421567	3042058	pt	Lichtenstein	1					
2421568	3042058	se	Liechtenstein	1					
2421569	3042058	sv	Lichtenstein	1					
2421570	3042058	ur	لیشٹنسٹائن	1					
2421571	3042058	vi	Lich-ten-xtên (Liechtenstein)	1					
2919904	3042058	link	https://en.wikipedia.org/wiki/Liechtenstein						
3055054	3042058	link	https://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%85%D1%82%D0%B5%D0%BD%D1%88%D1%82%D0%B5%D0%B9%D0%BD						
4332843	3042058	en	Principality of Liechtenstein						
7088281	3042058	ak	Lektenstaen	1					
7088282	3042058	az	Lixtenşteyn	1					
7088283	3042058	bm	Lisɛnsitayini	1					
7088284	3042058	bn	লিচেনস্টেইন	1					
7088285	3042058	ee	Litsenstein nutome	1					
7088286	3042058	ff	Lincenstayn	1					
7088287	3042058	gu	લૈચટેંસ્ટેઇન	1					
7088288	3042058	ha	Licansitan	1					
7088289	3042058	ki	Lishenteni	1					
7088290	3042058	kl	Liechtensteini	1					
7088291	3042058	kn	ಲಿಚೆನ್‌ಸ್ಟೈನ್	1					
7088292	3042058	ku	لیختنشتاین	1					
7088293	3042058	lg	Licitensitayini	1					
7088294	3042058	ln	Lishɛteni	1					
7088295	3042058	lu	Lishuteni	1					
7088296	3042058	mg	Listenstein	1					
7088297	3042058	ml	ലിച്ചൺസ്റ്റൈൻ	1					
7088298	3042058	mr	लिक्टेनस्टाइन	1					
7088299	3042058	nd	Liechtenstein	1					
7088300	3042058	ne	लिकटेन्सटाइन	1					
7088301	3042058	or	ଲିଚେଟନଷ୍ଟେଇନ୍	1					
7088302	3042058	rn	Lishyitenshitayini	1					
7088303	3042058	sg	Liechtenstein,	1					
7088304	3042058	sn	Liechtenstein	1					
7088305	3042058	so	Liyjtensteyn	1					
7088306	3042058	sw	Liechtenstein	1					
7088307	3042058	te	లిక్టెన్‌స్టెయిన్	1					
7088308	3042058	ti	ሊችተንስታይን	1					
7088309	3042058	to	Likitenisiteini	1					
7088310	3042058	yo	Lẹṣitẹnisiteni	1					
7088311	3042058	zu	i-Liechtenstein	1					
16925781	3042058	zh-Hant	列支敦斯登	1					
16929364	3042058	as	লিচটেনষ্টেইন	1					
16929365	3042058	ce	Лихтенштейн	1					
16929366	3042058	dz	ལིཀ་ཏནས་ཏ་ཡིན	1					
16929367	3042058	fy	Liechtenstein	1					
16929368	3042058	gd	Lichtenstein	1					
16929369	3042058	ig	Liechtenstein	1					
16929370	3042058	jv	Liktenstén	1					
16929371	3042058	kk	Лихтенштейн	1					
16929372	3042058	ks	لِکٹیسٹیٖن	1					
16929373	3042058	ky	Лихтенштейн	1					
16929374	3042058	mn	Лихтенштейн	1					
16929375	3042058	my	လစ်တန်စတိန်း	1					
16929376	3042058	pa	ਲਿਚੇਂਸਟਾਇਨ	1					
16929377	3042058	ps	لیختن اشتاین	1					
16929378	3042058	sd	لچي ٽينسٽين	1					
16929379	3042058	si	ලික්ටන්ස්ටයින්	1					
16929380	3042058	tg	Лихтенштейн	1					
16929381	3042058	tk	Lihtenşteýn	1					
16929382	3042058	tt	Лихтенштейн	1					
16929383	3042058	ug	لىكتېنستېين	1					
16929384	3042058	wo	Liktensteyin	1					
16929385	3042058	yi	ליכטנשטיין	1					
17095809	3042058	nuts	LI						
1298356	3042060		Hinterer Schellenberg						
1298357	3042060		Hinter-Schellenberg						
7065798	3042060	link	https://en.wikipedia.org/wiki/Hinterschellenberg						
1298358	3042062		Gambrin						
1298359	3042062		Gamprin-Bendern						
1914961	3042062	de	Gamprin						
1914962	3042062	es	Gamprin						
1914963	3042062	fr	Gamprin						
1914964	3042062	pl	Gamprin						
1914965	3042062	en	Gamprin						
1914966	3042062	ca	Gamprin						
1914967	3042062	eo	Gamprin						
1914968	3042062	it	Gamprin						
1914969	3042062	ja	ガンプリン						
1914970	3042062	nl	Gamprin						
2000597	3042062	eu	Gamprin						
3042376	3042062	ru	Гамприн						
3055055	3042062	link	https://ru.wikipedia.org/wiki/%D0%93%D0%B0%D0%BC%D0%BF%D1%80%D0%B8%D0%BD						
8103394	3042062	post	9487						
13786224	3042062	unlc	LIGAM						
16452576	3042062	de	Gamprin-Bendern						
1298360	3042063		Gamprin		1				
1298361	3042063		Gemeinde Gamprin-Bendern						
1298362	3042063		Gamprin-Bendern						
2968610	3042063	link	https://en.wikipedia.org/wiki/Gamprin						
5650180	3042063		Gemeinde Gamprin						
14914403	3042063	wkdt	Q49662						
17126318	3042063	lauc	LI00002003						
1909946	3042068	de	Eschen						
1909947	3042068	es	Eschen						
1909948	3042068	fr	Eschen						
1909949	3042068	pl	Eschen						
1909950	3042068	en	Eschen						
1909951	3042068	eo	Eschen						
1909952	3042068	it	Eschen						
1909953	3042068	ja	エッシェン						
1909954	3042068	nl	Eschen						
1909955	3042068	pt	Eschen						
1997468	3042068	ca	Eschen						
3055056	3042068	link	https://ru.wikipedia.org/wiki/%D0%AD%D1%88%D0%B5%D0%BD_%28%D0%9B%D0%B8%D1%85%D1%82%D0%B5%D0%BD%D1%88%D1%82%D0%B5%D0%B9%D0%BD%29						
8103398	3042068	post	9492						
13786223	3042068	unlc	LIESC						
1298365	3042069		Eschen-Nedeln						
1298366	3042069		Eschen		1				
1298367	3042069		Eschen / Nendeln						
2968611	3042069	link	https://en.wikipedia.org/wiki/Eschen						
5650181	3042069		Gemeinde Eschen						
8656423	3042069		Gemeinde Eschen-Nendeln						
8656424	3042069		Eschen-Nendeln						
9393551	3042069	be	Эшэн						
9393552	3042069	he	אשן						
9393553	3042069	ja	エッシェン						
9393554	3042069	ko	에셴						
9393555	3042069	lt	Ešenas						
9393556	3042069	pnb	ایشن						
9393557	3042069	th	เอสเชิน						
9393558	3042069	uk	Ешен						
9393559	3042069	ru	Эшен						
9393560	3042069	zh	埃申						
11468280	3042069	fa	اشن						
11468281	3042069	mk	Ешен						
14937644	3042069	wkdt	Q4540						
1298368	3042070		Drey Schwestern						
3075624	3042070	gsw	Drü-Schwöschtra	1					
7159992	3042072	link	https://en.wikipedia.org/wiki/Bendern						
14973738	3042072	wkdt	Q816776						
1906642	3042073	de	Balzers						
1906643	3042073	es	Balzers						
1906644	3042073	fr	Balzers						
1906645	3042073	pl	Balzers						
1906646	3042073	en	Balzers						
1906647	3042073	ca	Balzers						
1906648	3042073	eo	Balzers						
1906649	3042073	fi	Balzers						
1906650	3042073	it	Balzers						
1906651	3042073	ja	バルザース						
1906652	3042073	nl	Balzers						
1906653	3042073	pt	Balzers						
1995171	3042073	eu	Balzers						
3055057	3042073	link	https://ru.wikipedia.org/wiki/%D0%91%D0%B0%D0%BB%D1%8C%D1%86%D0%B5%D1%80%D1%81						
4332844	3042073		Balzers						
8103402	3042073	post	9496						
13786222	3042073	unlc	LIBAZ						
14889261	3042073	wkdt	Q27439073						
16241489	3042073	link	https://en.wikipedia.org/wiki/Balzers						
1298369	3042074		Balzers / Mäls						
1298370	3042074		Balzers		1				
5650182	3042074		Gemeinde Balzers						
8234443	3042074	link	https://en.wikipedia.org/wiki/Balzers						
14912497	3042074	wkdt	Q49663						
7063005	3315349	link	https://en.wikipedia.org/wiki/M%C3%A4ls						
7223731	3315349		Mäls						
15787115	3315350	wkdt	Q27439071						
16147017	3315350	link	https://en.wikipedia.org/wiki/Lawena						
1380560	3315351		Wangerbärg						
7171033	3315351	link	https://en.wikipedia.org/wiki/Wangerberg						
7035526	3315352	link	https://en.wikipedia.org/wiki/S%C3%BCcka						
1380561	3315353		Rotaboda						
7051345	3315353	link	https://en.wikipedia.org/wiki/Rotenboden						
7515987	3315354	link	https://en.wikipedia.org/wiki/Silum						
7577876	3315354		Silum						
15522551	3315354	wkdt	Q4191863						
7173388	3315356	link	https://en.wikipedia.org/wiki/Masescha						
7223732	3315356		Masescha						
7517804	3315357	link	https://en.wikipedia.org/wiki/Gaflei						
7577877	3315357		Gaflei						
1380562	3315358		Möliholz						
4793496	3315358		Mühleholz						
7072329	3315358	link	https://en.wikipedia.org/wiki/M%C3%BChleholz						
1380563	3315359		Ebaholz						
4793497	3315359		Ebenholz						
7071830	3315359	link	https://en.wikipedia.org/wiki/Ebenholz						
7134188	6545409	link	https://en.wikipedia.org/wiki/Malbun						
6900637	6545410	link	https://en.wikipedia.org/wiki/Steg_%28Liechtenstein%29						
15342379	6545410	wkdt	Q593158						
5713043	6938744	link	https://en.wikipedia.org/wiki/Plasteikopf						
5713035	6938746	link	https://en.wikipedia.org/wiki/Mittlerspitz						
3075625	6938759	gsw	Triesner-Mittagspitz	1	1				
3075698	6938779		Hochspieler						
3075700	6938790		Rappenstein						
5713012	6938790	link	https://en.wikipedia.org/wiki/Rappenstein						
5712775	6938803	link	https://en.wikipedia.org/wiki/Langspitz						
5712618	6938805	link	https://en.wikipedia.org/wiki/Koraspitz						
5712745	6938814	link	https://en.wikipedia.org/wiki/Goldlochspitz						
5712968	6938835	link	https://en.wikipedia.org/wiki/Spitz_%28Liechtenstein%29						
3075699	6938843		Kolmi						
5712743	6938856	link	https://en.wikipedia.org/wiki/Nospitz						
5712781	6938859	link	https://en.wikipedia.org/wiki/Heub%C3%BChl						
5712765	6938902	link	https://en.wikipedia.org/wiki/Kirchlespitz						
12251741	6938905		Krüppel						
12251522	6938920		Gamsgrad						
12251523	6938920		Gamsgrat	1					
16140243	6938920	link	https://en.wikipedia.org/wiki/Gamsgrat						
5712636	6938956	link	https://en.wikipedia.org/wiki/Stachlerkopf						
5713101	6938960	link	https://en.wikipedia.org/wiki/Drei_Kapuziner						
5712799	6938974	link	https://en.wikipedia.org/wiki/Sch%C3%B6nberg_%28Liechtenstein%29						
15523291	6939018	wkdt	Q27439052						
5712638	6939034	link	https://en.wikipedia.org/wiki/Helwangspitz						
3075668	6939061		Hechlekopf						
5712893	6939093	link	https://en.wikipedia.org/wiki/Gafleispitz						
16206293	6939093		Gafleichöpfli						
3075701	6939115		Kuhgrat						
3075702	6939115		Kuagrot						
5712990	6939115	link	https://en.wikipedia.org/wiki/Kuhgrat						
3076106	6939199		Sarojahöhe						
3076107	6939199		Saroya						
5712910	6939199	link	https://en.wikipedia.org/wiki/Sarojah%C3%B6he						
5713042	7303639	link	https://en.wikipedia.org/wiki/Garsellakopf						
5712915	7303643	link	https://en.wikipedia.org/wiki/Alpspitz						
16206302	7303643		Vorder Alpspitz						
3075696	7303721		Sami Chlaus						
3075697	7303721		Kläusle						
3075703	7303727		Guschasattel						
5713080	7303736	link	https://en.wikipedia.org/wiki/Augstenberg_%28Liechtenstein%29						
3075704	7303739		Hubel						
5713057	7303739	link	https://en.wikipedia.org/wiki/Silberhorn_%28Liechtenstein%29						
14928716	10280529	wkdt	Q1792561						
16205380	10280529	link	https://en.wikipedia.org/wiki/Kunstmuseum_Liechtenstein						
16160874	10280530	link	https://en.wikipedia.org/wiki/Liechtenstein_National_Museum						
10772345	10280532	en	Stamp Museum						
16163045	10280540	link	https://en.wikipedia.org/wiki/Sportanlage_Rheinwiese						
11401894	10347324	de	Bangserfeld						
11401895	10347325	de	Bangserwesa						
11401896	10347326	de	Lettagiessa						
11401897	10347327	de	Evimeder						
11401898	10347329	de	Köbelesmeder						
11401899	10347330	de	Schneggenäuele						
11401900	10347351	de	Wolfert						
11401901	10347352	de	Under Wesa						
11401902	10347353	de	Regelmeder						
11401903	10347356	de	Spierswesa						
11401904	10347357	de	Tüfagraba						
11401905	10347374	de	Feschera						
11401906	10347375	de	Langammet						
10859159	10347376		Bierkileteile						
10859160	10347376		Birkileteile						
11401908	10347429	de	Flandera						
11401909	10347430	de	Brema						
11401910	10347431	de	Tüfmeder						
11401911	10347432	de	Halameder						
10859226	10347433		Ruine Alt Schellenberg						
10859227	10347433		Burgruine Untere Schellenberg						
10859228	10347436		Steinbroch						
10859229	10347436		Deponie Limseneck						
11401912	10347437	de	Kela						
11401913	10347444	de	Wüerle						
11401914	10347445	de	Rüttile						
11401915	10347452	de	Scherer						
11401916	10347453	de	Oksarietle						
11401917	10347454	de	Tuarbariet						
11401918	10347501	de	Barietle						
11401919	10347502	de	Röfeteile						
11401920	10347503	de	Birka						
11401921	10347504	de	Schmelzhof						
11401922	10347568	de	Auäcker						
11401923	10347584	de	Jederfeld						
11401924	10347585	de	Simmasguet						
11401925	10347586	de	Äule						
16195968	10347587	link	https://en.wikipedia.org/wiki/Gampriner_Seele						
16222593	10347587		Gampriner Seele						
10859368	10347591		Michel Öhri						
10859369	10347591		Mechel Öhri						
10859370	10347595		Lomgruebböchel						
10859371	10347595		Lomgrueb						
11401928	10347608	de	Stegamädle						
11401929	10347610	de	Mölemad						
11401930	10347612	de	Under Wesa						
13786228	10347616	unlc	LISCH						
13830280	10347616		Hinterer Schaanwald						
11401931	10347619	de	Langmad						
10859394	10347620		Wert						
10859395	10347620		Werth						
10944929	10347623		Rütteler						
10944930	10347623		Rütteböchel						
16801367	10347628	de	Kloster Schellenberg						
11401932	10347638	de	Winkel						
11401933	10347640	de	Kaliforneteile						
11401934	10347641	de	Hans Kinds Mad						
11401935	10347649	de	Oberau						
11403656	10348129	de	Mölimad						
11403663	10348130	de	Böscha						
10860014	10348137		Inner Schaffert						
10860015	10348137		Innera Schafflet						
11403797	10348139	de	Erlabretscha						
11403817	10348261	de	Gross Bretscha						
11403819	10348262	de	Rormeder						
11403830	10348264	de	Altenöderszepfel						
11403831	10348266	de	Alt Teilega						
11403832	10348267	de	Tentsche						
11403833	10348268	de	Teiliga						
10944933	10348269		Scheidgraba						
10944934	10348269		Grenzgraba						
11403834	10348272	de	Retterliszepfel						
11403835	10348273	de	Tentscha						
11403836	10348275	de	Schefflände						
16150951	10348287	link	https://en.wikipedia.org/wiki/Gutenberg_Castle						
16222646	10348287		Burg Gutenberg						
11403849	10348303	de	Tschingel						
11403854	10348304	de	Oberfeld						
11403856	10348306	de	Neugüeter						
11403857	10348307	de	Altneuguet						
11404018	10348417	de	Pfaffamad						
11404020	10348418	de	Äscherle						
11404042	10348433	de	Loma						
11404043	10348436	de	Weslewolf						
11404044	10348438	de	Bofel						
10860343	10348553		Herawingert						
10860344	10348553		Bockwingert						
11404417	10348557	de	Haberfeld						
10944935	10348567		Schloss Vaduz						
10944936	10348567		Fürstenhaus von Liechtenstein						
10944937	10348567		Haus Liechtenstein						
16147881	10348567	link	https://en.wikipedia.org/wiki/Vaduz_Castle						
11404421	10348569	de	Irkeles						
11407868	10351430	de	Suwinkel						
11407871	10351432	de	Streuteile						
11407872	10351435	de	Altneufeld						
11407873	10351437	de	Neuwesa						
11407874	10351441	de	Dammwesa						
11407876	10351442	de	Fuksera						
11407971	10351531	de	Neufeld						
11407972	10351533	de	Neuland						
13545357	10351534	de	Platta						
11407973	10351535	de	Brüel						
11407974	10351536	de	Ganada						
11407975	10351537	de	Streuiriet						
11407976	10351558	de	Hausteile						
11407977	10351559	de	Grosse Teile						
11407978	10351560	de	Tuarbateil						
11407979	10351561	de	Langmad						
11407980	10351562	de	Pfarmeder						
11407981	10351563	de	Underau						
11407983	10351564	de	Fenkeri						
11407985	10351565	de	Bofelgätterle						
11407986	10351566	de	Schafwinkel						
11407988	10351568	de	Mederschlatt						
11407991	10351570	de	Orglamad						
11407992	10351573	de	Uf da Gräba						
11407995	10351596	de	Fukseri						
11407996	10351597	de	Schaaneri						
11407997	10351599	de	Meder						
11407998	10351601	de	Eicha						
11407999	10351602	de	Bofel						
11408000	10351604	de	Grabaton						
11408001	10351606	de	Egelgraba						
11408002	10351611	de	Gmeindswesa						
11408003	10351612	de	Underem Damm						
11408029	10351615	de	Schwizerwesa						
11408059	10351617	de	Wesa						
11408099	10351618	de	Rüttile						
11408503	10351836	de	Ober Riet						
11408569	10351988	de	Undera Bretscha						
11408571	10351989	de	Rietfeld						
10866469	10351990		Kretzera						
10866470	10351990		Krezara						
11408592	10352015	de	Altastetner Meder						
11408593	10352016	de	Specketeile						
11408594	10352017	de	Specke						
11408595	10352018	de	Haslermad						
11408597	10352026	de	Rietle						
10866536	10352029		Kalchofa						
10866537	10352029		Kalkofa						
11408599	10352102	de	Krutgärta						
10866794	10352171		Mösmafeld						
10866795	10352171		Mösmerfeld						
11408638	10352220	de	Bruggmeder						
11408639	10352223	de	Seelemeder						
11408642	10352230	de	Teilega						
11408643	10352231	de	Ober Atzig						
11408652	10352294	de	Armaguet						
11408653	10352297	de	Jörlismad						
11408707	10352343	de	Jodameder						
11408708	10352344	de	Bameder						
11408709	10352345	de	Rossriettli						
11408710	10352347	de	Rietteile						
11408711	10352348	de	Familienteile						
11408712	10352349	de	Undermad						
11408713	10352352	de	Meder						
11408714	10352353	de	Kranzamad						
10867142	10352375		Nendlerröfi						
10867143	10352375		Nendler Röfi						
10867144	10352376		Osser Schaffert						
10867145	10352376		Ossera Schafflet						
10944947	10352377		Schwabbrünna						
10944948	10352377		Bim Steinbroch						
11408737	10352410	de	Rütti						
11408750	10352461	de	Au						
11408755	10352465	de	Lett						
11408756	10352466	de	Ober Schaanerau						
11408772	10352467	de	Schaanerau						
11408773	10352468	de	Neufeld						
11408774	10352478	de	Rütti						
11408775	10352479	de	Rüttili						
11408802	10352522	de	Hindervalorsch						
11409422	10353002	de	Krüzletola						
11409525	10353072	de	Notz						
10944951	10353142		Plankner Garselli						
10944952	10353142		Garselli						
10868314	10353170		Nägelehötta						
10868315	10353170		Negelehötta						
11409603	10353234	de	Güschgle						
11409604	10353254	de	Hinderem Kirchle						
11409605	10353259	de	Vordere Länge						
10944953	10353260		Vaduzer Riet						
10944954	10353260		Riet						
11409606	10353261	de	Torbateil						
11409607	10353262	de	Kopfteil						
11409608	10353263	de	Grossriet						
11409609	10353266	de	Riet						
11409613	10353273	de	Alt Riet						
16147593	10353315	link	https://en.wikipedia.org/wiki/Maurerberg						
16222705	10353315		Maurerberg						
11409683	10353367	de	Fuermares						
11410033	10353678	de	Rüti						
10869170	10353783		Lerchabödali						
10869171	10353783		Lerchabodeli						
11410115	10353886	de	Obersäss						
15574124	10353911	wkdt	Q27439070						
16058125	10353911		Valüna						
16189035	10353911	link	https://en.wikipedia.org/wiki/Val%C3%BCna						
11410169	10354403	de	Demmeraböchel						
11410201	10354583	de	Welda Bongert						
11410202	10354590	de	Neugrütt						
10944957	10354613		Seilerkopf						
10944958	10354613		Sepp Nägili Kopf						
11410207	10354625	de	Undera Hälos						
11410208	10354628	de	Obera Hälos						
11410209	10354631	de	Gartnerisch						
11410211	10354644	de	Im Damm						
11410212	10354645	de	Trachter						
11410272	10354849	de	Meisbädli						
11410278	10354885	de	Waldgarta						
11410279	10354888	de	Krottaloch						
11410280	10354890	de	Insili						
11410300	10354947	de	Auteil						
11410301	10354948	de	Neusand						
11410303	10354949	de	Sandteil						
10944963	10355060		Sareis						
10944964	10355060		Uberem Grad						
11410368	10355163	de	Chüaloch						
11410371	10355186	de	Sassförkle						
10944965	10355192		Sassstall						
10944966	10355192		Sass-Stall						
11410375	10355196	de	Stachler						
11410540	10356400	de	Mittlervalorsch						
11410541	10356416	de	Garsälli						
10944972	10356714		Jagdhus						
10944973	10356714		Gafadurahütte						
11410645	10357047	de	Vogelsang						
11410646	10357048	de	Under Neugrütt						
11410647	10357049	de	Ober Neugrütt						
11410648	10357050	de	Rheinbruch						
11410649	10357051	de	Neugrütt						
11410651	10357094	de	Stadelbach						
11410652	10357098	de	Aviols						
11410653	10357101	de	Dieplatza						
11410654	10357103	de	Witriet						
11410655	10357104	de	Ferler						
11410656	10357111	de	Büelkappili						
11410657	10357116	de	Rheinau						
11410658	10357117	de	Rüttena						
11410659	10357118	de	Husteil						
11410664	10357146	de	Under Husteil						
11410665	10357147	de	Ober Husteil						
11410666	10357148	de	Bi der Möle						
11410667	10357149	de	Underfeld						
11410669	10357150	de	Osser der Möle						
11410670	10357151	de	Insel						
11410671	10357152	de	Riet						
11410672	10357155	de	Mölebach						
11410673	10357156	de	Lehawes						
11410675	10357161	de	Torbariet						
11410678	10357168	de	Mura						
11410679	10357169	de	Äule						
11410680	10357170	de	Äuleheg						
11411743	10366884	de	Färchaloch						
10892667	10366942		Bipfal						
10892668	10366942		Bitfal						
10944984	10366951		Bargälla						
10944985	10366951		Alpa						
11411755	10366960	de	Schlüachtachopf						
11411756	10366966	de	Kapferstentscha						
11411757	10366967	de	Kanalmeder						
11411758	10366969	de	Auteil						
11411789	10367019	de	Lang Teil						
11411790	10367020	de	Kopfteil						
11411791	10367021	de	Mittlere Länge						
11411792	10367022	de	Hintere Länge						
11411793	10367023	de	Weidriet						
11411794	10367024	de	Mittlere Teile						
11411795	10367026	de	Wisanels						
11411796	10367027	de	Riet						
11411797	10367028	de	Ober Riet						
11411799	10367029	de	Kleine Teile						
11411804	10367045	de	Kolmad						
11411806	10367046	de	In den Teilen						
11411807	10367048	de	Egertmäder						
11411809	10367061	de	Rischamad						
11411810	10367062	de	Streueteile						
11411811	10367063	de	Alte Teile						
11411813	10367064	de	Lange Teile						
11411814	10367065	de	Buechastein						
11411815	10367066	de	Spiersteile						
11411816	10367068	de	Hasabachmeder						
11411818	10367069	de	Spielteile						
11411819	10367070	de	Feschera						
11411820	10367071	de	Rietgartawesa						
11411821	10367072	de	Melcheichawesa						
11411822	10367073	de	Bretscha						
11411823	10367075	de	Schlatt						
11411824	10367077	de	Wesle						
11411826	10367087	de	Grosse Teile						
11411827	10367099	de	Kleiner Kanal						
10892679	10367102		Gaschlores						
10892680	10367102		Gaschlo-Res						
11411828	10367104	de	Äule						
11411829	10367112	de	Neuguet						
10907727	10378577	de	Triesenberg, Rizlina						
10907879	10378729	de	Gamprin, Unterbühl						
10908005	10378855	de	Schaan, Forst/Hilti						
10908055	10378905	de	Ruggell, Industriering						
10908385	10379236	de	Triesenberg, Almeina						
10908409	10379260	de	Mauren FL, Freihof						
11412792	10408741	de	Gärta						
11412795	10408750	de	Familienteile						
11412796	10408751	de	Riet						
11412797	10408753	de	Teile						
11412798	10408755	de	Rheinwesen						
11412799	10408757	de	Soldatenteile						
16189114	10412985	link	https://en.wikipedia.org/wiki/Liechtensteinisches_Gymnasium						
16222717	10412985		Liechtensteinisches Gymnasium						
11431806	11073942	de	Bärgällasattel						
11432092	11074220	de	Chemi						
11433060	11075150	de	Mattaförkle						
16385799	12069846		Velobrücke Buchs / Vaduz						
