1456721	3580473		South West Point						
4332746	3580473		West End Point						
1456722	3580474		South West Point						
4332747	3580474		West End Point						
1456723	3580477		West End						
4332748	3580477		West Bay						
7148295	3580477	link	https://en.wikipedia.org/wiki/West_Bay%2C_Cayman_Islands						
7323791	3580478		West Bay						
15378573	3580487	wkdt	Q27346975						
15969305	3580487		Vidal Cay						
15320668	3580493	wkdt	Q27346973						
15969306	3580493		Timms Point						
8695483	3580499	link	https://en.wikipedia.org/wiki/The_Bluff_%28Cayman_Islands%29						
1456724	3580503		Stakes Bay Point						
4332749	3580503		Stake Bay Point						
15878454	3580503	wkdt	Q27346971						
1456725	3580504		Stakes Bay						
4332750	3580504		Stake Bay						
1456726	3580505		Spots Bay						
4332751	3580505		Spotts Bay						
1456727	3580518		Blossom Village						
1456728	3580518		South Hole						
4332752	3580518		South Town						
1456729	3580519		South West Sound						
4332753	3580519		South Sound						
1456730	3580527		Scotts Bay						
4332754	3580527		Scotts Anchorage						
1456731	3580530		Sand Cliff Point						
4332755	3580530		Sandy Point						
15799429	3580536	wkdt	Q27347482						
15969307	3580536		Salt Water Point						
1456732	3580544		Roger Wreck Point						
4332756	3580544		Rogers Wreck Point						
15589121	3580544	wkdt	Q27347478						
1456733	3580547		Red Bay						
4332757	3580547		Red Bay Estate						
1456734	3580560	en	Georgetown Owen Roberts International Airport						
1892567	3580560	icao	MWCR						
1892568	3580560	iata	GCM						
2986230	3580560	link	https://en.wikipedia.org/wiki/Owen_Roberts_International_Airport						
5894132	3580560	es	Aeropuerto Internacional Owen Roberts						
5894133	3580560	it	Aeroporto Internazionale Owen Roberts						
5894134	3580560	vi	Sân bay quốc tế Owen Roberts						
5894135	3580560	zh	歐文·羅伯茨國際機場						
5894136	3580560	ms	Lapangan Terbang Antarabangsa Roberts						
5894137	3580560	pl	Port lotniczy Owen Roberts						
8545179	3580560	fr	Aéroport international Owen Roberts de Georgetown						
8545180	3580560	fa	فرودگاه بین‌المللی اوون رابرتس						
11620797	3580560	id	Bandar Udara Internasional Owen Roberts						
11620798	3580560	pt	Aeroporto Internacional Owen Roberts						
13786067	3580560	unlc	KYGCM						
14947390	3580565	wkdt	Q27347468						
15969308	3580565		Old Robin Point						
1456735	3580566		Old Man Village						
4332758	3580566		Old Man Bay						
1456736	3580573		Grand Sound						
4332759	3580573		North Sound						
1456737	3580575		North Side Village						
4332760	3580575		North Side						
16169097	3580575	link	https://en.wikipedia.org/wiki/North_Side%2C_Cayman_Islands						
1456738	3580580		Newland						
4332761	3580580		Newlands						
8677592	3580595	link	https://en.wikipedia.org/wiki/Malportas_Pond						
8802928	3580595		Malportas Pond						
15769816	3580596	wkdt	Q27347451						
15969309	3580596		Mallarbs Point						
1456739	3580607		Long Coconut Point						
4332762	3580607		Long Coconut Tree Point						
15879063	3580607	wkdt	Q27347442						
15341592	3580609	wkdt	Q27347441						
15969310	3580609		Little Spot Point						
15222777	3580613	wkdt	Q27347438						
15969311	3580613		Little Cayman Brac						
2986231	3580614	link	https://en.wikipedia.org/wiki/Little_Cayman						
3056570	3580614	link	https://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D0%BB%D1%8B%D0%B9_%D0%9A%D0%B0%D0%B9%D0%BC%D0%B0%D0%BD						
4332763	3580614		Little Cayman						
8575063	3580614	en	Little Cayman						
8575066	3580614	iata	LYB						
8575151	3580614	yue	小开曼						
8575158	3580614	ja	リトルケイマン						
8575175	3580614	es	Pequeña Caimán						
8575188	3580614	ru	Малый Кайман						
8575189	3580614	fr	Little Cayman						
11549163	3580614	be	Востраў Літл-Кайман						
11549164	3580614	el	Λιτλ Κέιμαν						
11549165	3580614	gl	Caimán Menor						
11549166	3580614	he	ליטל קיימן						
11549167	3580614	hbs	Mali Kajman						
11549168	3580614	uk	Малий Кайман						
11549169	3580614	pl	Mały Kajman						
11549170	3580614	pt	Pequena Caimão						
11549171	3580614	zh	小开曼						
15795108	3580614	wkdt	Q1141722						
1456740	3580624		Juniper Bay						
4332764	3580624		Jennifer Bay						
1456741	3580627		Jackson Point						
4332765	3580627		Jackson’s Point						
15670254	3580631	wkdt	Q27347423						
15969312	3580631		Ironshore Point						
15749570	3580635	wkdt	Q27347421						
15969313	3580635		High Rock Point						
15509686	3580642	wkdt	Q27347419						
15969314	3580642		Head of Barkers						
1456742	3580650		Gun Bay Village						
4332766	3580650		Gun Bay						
15467017	3580651	wkdt	Q27347416						
15969315	3580651		Great Pedro Point						
15681462	3580654	wkdt	Q27347415						
15969316	3580654		Grape Tree Point						
2986232	3580656	link	https://en.wikipedia.org/wiki/Grand_Cayman						
3056571	3580656	link	https://ru.wikipedia.org/wiki/%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%BE%D0%B9_%D0%9A%D0%B0%D0%B9%D0%BC%D0%B0%D0%BD						
4332767	3580656		Grand Cayman						
8545182	3580656	en	Grand Cayman						
8545183	3580656	iata	GCM						
1456743	3580660	en	Cayman Brac Airport				1		
4332768	3580660	en	Sir Charles Kirkconnell International Airport						
5905835	3580660	link	https://en.wikipedia.org/wiki/Charles_Kirkconnell_International_Airport						
5905836	3580660	icao	MWCB						
5905837	3580660	iata	CYB						
5905838	3580660	en	Gerrard Smith International Airport				1		
1596062	3580661	de	George Town						
1596063	3580661	en	George Town						
1596064	3580661	fi	George Town						
1596065	3580661	io	Georgetown						
1596066	3580661	pl	Georgetown						
1596067	3580661	sv	George Town						
1649412	3580661	fr	George Town						
1649450	3580661	it	George Town						
1649510	3580661	da	George Town						
1649633	3580661	el	George Town						
1649739	3580661	es	George Town						
1649811	3580661	pt	George Town						
1907069	3580661	ca	George Town						
1907070	3580661	he	גורג טאון						
1907071	3580661	id	George Town						
1907072	3580661	ja	ジョージタウン						
1907073	3580661	nl	Georgetown						
1907074	3580661	sk	George Town						
1994089	3580661	nl	George Town						
1994090	3580661	zh-TW	喬治敦						
2986233	3580661	link	https://en.wikipedia.org/wiki/George_Town%2C_Cayman_Islands						
8545181	3580661	iata	GCM						
8715105	3580661	af	Georgetown						
8715106	3580661	ar	جورج تاون						
8715107	3580661	az	Corctaun						
8715108	3580661	be	Джорджтаўн						
8715109	3580661	bg	Джордж Таун						
8715110	3580661	el	Τζωρτζ Τάουν						
8715111	3580661	eo	Georĝurbo						
8715112	3580661	ka	ჯორჯტაუნი						
8715113	3580661	ko	조지타운						
8715114	3580661	lt	Džordžtaunas						
8715115	3580661	mk	Џорџ Таун						
8715116	3580661	mr	जॉर्जटाउन						
8715117	3580661	nn	George Town på Caymanøyane						
8715118	3580661	sr	Џорџтаун						
8715119	3580661	th	จอร์จทาวน์						
8715120	3580661	uk	Джорджтаун						
8715121	3580661	ur	جارج ٹاؤن، جزائر کیمین						
8715122	3580661	war	Georgetown						
8715123	3580661	ru	Джорджтаун						
8715124	3580661	zh	喬治敦						
11344877	3580661	fa	جرج‌تاون، جزایر کیمن						
11344878	3580661	ta	ஜார்ஜ் டவுன்						
13786068	3580661	unlc	KYGEC						
1456744	3580670		Fisherman Rock						
4332769	3580670		Fisherman’s Rock						
15278601	3580674	wkdt	Q27347409						
1456745	3580677		East Channel						
4332770	3580677		East End Channel						
1456746	3580678		Old Isaacs						
4332771	3580678		East End						
8675947	3580678	link	https://en.wikipedia.org/wiki/East_End%2C_Cayman_Islands						
15379695	3580680	wkdt	Q27347406						
15969317	3580680		Duck Pond Cay						
15780894	3580687	wkdt	Q27347402						
15969318	3580687		Diggarys Point						
1456747	3580704		East Point						
4332772	3580704		Colliers Point						
15892643	3580711	wkdt	Q27347386						
15969319	3580711		Clarinda Beach						
1456748	3580712		Charles Bay						
4332773	3580712		Charles Bight						
1456749	3580718		Cayman						
1632494	3580718	es	Islas Caimán	1					
2256641	3580718	fr	Îles Caïmans	1					
2256642	3580718	fr	Îles Caïmanes						
2256643	3580718	fr	Îles Caïman						
2421625	3580718	af	Kaaimanseilande	1					
2421626	3580718	am	ካይማን ደሴቶች	1					
2421627	3580718	ar	جزر الكايمن	1					
2421628	3580718	be	Кайманавы астравы	1					
2421629	3580718	bg	Кайманови острови	1					
2421630	3580718	bn	কেম্যান দ্বীপপুঞ্জ	1					
2421631	3580718	bo	ཁེ་མེན་གླིང་ཕྲན།	1					
2421632	3580718	ca	Illes Caiman	1					
2421633	3580718	cs	Kajmanské ostrovy	1					
2421634	3580718	cy	Ynysoedd Cayman	1					
2421635	3580718	da	Caymanøerne	1					
2421636	3580718	de	Kaimaninseln	1					
2421637	3580718	el	Νήσοι Κέιμαν	1					
2421638	3580718	en	Cayman Islands	1					
2421639	3580718	eo	Kejmanoj	1					
2421640	3580718	et	Kaimanisaared	1					
2421641	3580718	eu	Kaiman Uharteak	1					
2421642	3580718	fa	جزایر کِیمن	1					
2421643	3580718	fi	Caymansaaret	1					
2421644	3580718	fr	Iles Caïmanes						
2421645	3580718	ga	Oileáin Cayman	1					
2421646	3580718	gl	Illas Caimán	1					
2421647	3580718	he	איי קיימן	1					
2421648	3580718	hi	कैमेन द्वीपसमूह	1					
2421649	3580718	hr	Kajmanski Otoci	1					
2421650	3580718	hu	Kajmán-szigetek	1					
2421651	3580718	ia	Insulas de Caiman	1					
2421652	3580718	id	Kepulauan Kayman	1					
2421653	3580718	is	Caymaneyjar	1					
2421654	3580718	it	Isole Cayman	1					
2421655	3580718	ja	ケイマン諸島	1					
2421656	3580718	ka	კაიმანის კუნძულები	1					
2421657	3580718	ko	케이맨제도	1					
2421658	3580718	lt	Kaimanų salos	1					
2421659	3580718	lv	Kaimanu salas	1					
2421660	3580718	mk	Кајмански Острови	1					
2421661	3580718	ms	Cayman Islands	1					
2421662	3580718	mt	il-Gżejjer Cayman	1					
2421663	3580718	nb	Caymanøyene	1					
2421664	3580718	nl	Caymaneilanden	1					
2421665	3580718	nn	Caymanøyane	1					
2421666	3580718	pl	Kajmany	1					
2421667	3580718	pt	Ilhas Caiman	1					
2421668	3580718	ro	Insulele Cayman	1					
2421669	3580718	ru	Каймановы Острова	1					
2421670	3580718	sk	Kajmanské ostrovy	1					
2421671	3580718	sl	Kajmanski otoki	1					
2421672	3580718	sr	Кајманска Острва	1					
2421673	3580718	sv	Caymanöarna	1					
2421674	3580718	th	หมู่เกาะเคย์แมน	1					
2421675	3580718	tr	Cayman Adaları	1					
2421676	3580718	uk	Кайманові острови	1					
2421677	3580718	ur	کیمین آئلینڈز	1					
2421678	3580718	vi	Quần Đảo Cayman	1					
2421679	3580718	zh	开曼群岛	1					
4332774	3580718		Cayman Islands						
5423948	3580718	link	https://en.wikipedia.org/wiki/Cayman_Islands						
7088114	3580718	ak	Kemanfo Islands	1					
7088115	3580718	az	Kayman Adaları	1					
7088116	3580718	bm	Bama Gun	1					
7088117	3580718	br	Inizi Cayman	1					
7088118	3580718	bs	Kajmanska Ostrva	1					
7088119	3580718	ee	Kayman ƒudomekpowo nutome	1					
7088120	3580718	ff	Duuɗe Kaymaa	1					
7088121	3580718	fo	Caymanoyggjarnar	1					
7088122	3580718	gu	કેમેન આઇલેન્ડ્સ	1					
7088123	3580718	ha	Tsibiran Kaiman	1					
7088124	3580718	ki	Visiwa vya Kayman	1					
7088125	3580718	kn	ಕೇಮನ್ ದ್ವೀಪಗಳು	1					
7088126	3580718	lg	Bizinga ebya Kayimaani	1					
7088127	3580718	ln	Bisanga bya Kayíma	1					
7088128	3580718	lu	Lutanda lua Kayima	1					
7088129	3580718	mg	Nosy Kayman	1					
7088130	3580718	ml	കേയ്മാൻ ദ്വീപുകൾ	1					
7088131	3580718	mr	केमन बेटे	1					
7088132	3580718	my	ကေမန် ကျွန်းစု	1					
7088133	3580718	nd	Cayman Islands	1					
7088134	3580718	ne	केयमान टापु	1					
7088135	3580718	oc	Islas Caiman	1					
7088136	3580718	or	କେମ୍ୟାନ୍‌ ଦ୍ୱୀପପୁଞ୍ଜ	1					
7088137	3580718	rm	Inslas Cayman	1					
7088138	3580718	rn	Ibirwa bya Keyimani	1					
7088139	3580718	se	Cayman-sullot	1					
7088140	3580718	sg	Âzûâ Ngundë, Kaimäni	1					
7088141	3580718	sn	Zvitsuwa zveCayman	1					
7088142	3580718	so	Cayman Islands	1					
7088143	3580718	sw	Visiwa vya Cayman	1					
7088144	3580718	ta	கெய்மென் தீவுகள்	1					
7088145	3580718	te	కేమాన్ దీవులు	1					
7088146	3580718	ti	ደሴታት ኬይማን	1					
7088147	3580718	to	ʻOtumotu Keimeni	1					
7088148	3580718	yo	Etíokun Kámánì	1					
7088149	3580718	zu	i-Cayman Islands	1					
8700548	3580718	nl	Kaaimaneilanden	1					
8700549	3580718	ak	Cayman Supɔws						
8700550	3580718	als	Kaimaninseln						
8700551	3580718	an	Islas Caimán						
8700552	3580718	ang	Cægman Īegland						
8700553	3580718	ar	جزر كايمان	1					
8700554	3580718	ast	Islles Caimán						
8700555	3580718	az	Kayman adaları						
8700556	3580718	bpy	কায়ম্যান দ্বীপমালা						
8700557	3580718	bs	Kajmanska ostrva						
8700558	3580718	dsb	Kajmany						
8700559	3580718	dv	ކޭމަން ޖަޒީރާ						
8700560	3580718	el	Κέιμαν Νήσοι						
8700561	3580718	eo	Kajmana Insularo						
8700562	3580718	eu	Kaiman uharteak						
8700563	3580718	ext	Islas Caimán						
8700564	3580718	fa	جزایر کیمن						
8700565	3580718	fo	Caymanoyggjar	1					
8700566	3580718	frp	Iles Cayimans						
8700567	3580718	frr	Kaiman Eilunen						
8700568	3580718	hi	केमन द्वीपसमूह						
8700569	3580718	hr	Kajmanski otoci						
8700570	3580718	hy	Կայմանյան կղզիներ	1					
8700571	3580718	id	Kepulauan Cayman	1					
8700572	3580718	io	Insuli Kaiman						
8700573	3580718	jv	Kapuloan Cayman						
8700574	3580718	kk	Кайман аралдары	1					
8700575	3580718	ko	케이맨 제도	1					
8700576	3580718	kw	Ynysow Kayman						
8700577	3580718	la	Insulae Caimanenses						
8700578	3580718	lb	Kaimaninselen	1					
8700579	3580718	li	Kaojmaanseilen						
8700580	3580718	lij	Isoe Cayman						
8700582	3580718	mr	केमन द्वीपसमूह						
8700583	3580718	ms	Kepulauan Cayman	1					
8700584	3580718	nds	Kaimaninseln						
8700585	3580718	no	Caymanøyene	1					
8700586	3580718	nov	Kayman Isles						
8700587	3580718	oc	Illas Caiman						
8700588	3580718	or	କେମନ୍ ଦ୍ଵୀପପୁଞ୍ଜ						
8700589	3580718	os	Кайманы сакъадæхтæ						
8700590	3580718	pa	ਕੇਮਨ ਟਾਪੂ						
8700591	3580718	pms	Ìsole Caiman						
8700592	3580718	pnb	کیمین جزیرے						
8700593	3580718	rw	Ibirwa bya Kayimani						
8700594	3580718	hbs	Kajmanski Otoci						
8700595	3580718	sk	Kajmanie ostrovy	1					
8700596	3580718	sq	Ishujt Cayman						
8700597	3580718	su	Kapuloan Kayman						
8700598	3580718	ta	கேமன் தீவுகள்						
8700599	3580718	tl	Kapuluang Kayman						
8700600	3580718	ug	كايمان تاقىم ئاراللىرى						
8700601	3580718	ur	جزائر کیمین						
8700602	3580718	vi	Quần đảo Cayman						
8700603	3580718	war	Kapuropud-an Cayman						
8700604	3580718	yo	Àwọn Erékùṣù Káímàn						
8700605	3580718	nan	Cayman Kûn-tó						
8700606	3580718	yue	開曼羣島						
8700607	3580718	pt	Ilhas Cayman	1					
8700608	3580718	ru	Острова Кайман	1					
8700609	3580718	zh	開曼群島						
11323629	3580718	arz	جزر كايمان						
11323630	3580718	ba	Кайман утрауҙары						
11323631	3580718	cdo	Cayman Gùng-dō̤						
11323632	3580718	ce	Кайманийн гӀайренаш						
11323633	3580718	ckb	دوورگەکانی کەیمەن						
11323634	3580718	cy	Ynysoedd Caiman						
11323635	3580718	gag	Kayman Adaları						
11323636	3580718	mrj	Кайман ошмаотывлӓ						
11323637	3580718	uz	Kayman orollari	1					
16925776	3580718	zh-Hant	開曼群島	1					
16929258	3580718	as	কেইমেন দ্বীপপুঞ্জ	1					
16929259	3580718	ce	Кайман гӀайренаш	1					
16929260	3580718	dz	ཁེ་མེན་གླིང་ཚོམ	1					
16929261	3580718	fy	Caymaneilannen	1					
16929262	3580718	gd	Na h-Eileanan Caimean	1					
16929263	3580718	ig	Agwaetiti Cayman	1					
16929264	3580718	jv	Kapuloan Kéman	1					
16929265	3580718	kl	Cayman qeqertaq	1					
16929266	3580718	km	កោះ​កៃម៉ង់	1					
16929267	3580718	ks	کیمَن جٔزیٖرٕ	1					
16929268	3580718	ku	Giravên Kaymanê	1					
16929269	3580718	ky	Кайман аралдары	1					
16929270	3580718	lo	ໝູ່ເກາະ ເຄແມນ	1					
16929271	3580718	mn	Кайманы арлууд	1					
16929272	3580718	pa	ਕੇਮੈਨ ਟਾਪੂ	1					
16929273	3580718	ps	کیمان ټاپوګان	1					
16929274	3580718	qu	Islas Caimán	1					
16929275	3580718	sd	ڪي مين ٻيٽ	1					
16929276	3580718	si	කේමන් දූපත්	1					
16929277	3580718	sq	Ishujt Kajman	1					
16929278	3580718	tg	Ҷазираҳои Кайман	1					
16929279	3580718	tk	Kaýman adalary	1					
16929280	3580718	tt	Кайман утраулары	1					
16929281	3580718	ug	كايمان ئاراللىرى	1					
16929282	3580718	wo	Duni Kaymaŋ	1					
16929283	3580718	yi	קיימאַן אינזלען	1					
1456750	3580719	en	The Caymans						
1632495	3580719	es	Islas Caimán						
4332775	3580719		Cayman Islands						
6129076	3580719		The Caymans						
2986234	3580720	link	https://en.wikipedia.org/wiki/Cayman_Brac						
3056572	3580720	link	https://ru.wikipedia.org/wiki/%D0%9A%D0%B0%D0%B9%D0%BC%D0%B0%D0%BD-%D0%91%D1%80%D0%B0%D0%BA						
4332776	3580720		Cayman Brac						
8567283	3580720	iata	CYB						
8567284	3580720	en	Cayman Brac Island						
8567285	3580720	fr	Île de Cayman Brac						
15208903	3580727	wkdt	Q27347378						
15969320	3580727		Bowse Land						
1456751	3580733		Bodden						
4332777	3580733		Bodden Town						
7140590	3580733	link	https://en.wikipedia.org/wiki/Bodden_Town_%28village%29						
15248994	3580733	wkdt	Q524789						
15619849	3580740	wkdt	Q27347370						
15515388	3580744	wkdt	Q27347368						
15969321	3580744		Bill Eden Point						
15213315	3580749	wkdt	Q27347364						
15969322	3580749		Betty Bay Point						
15597719	3580750	wkdt	Q27347363						
15969323	3580750		Bessy Howard Cay						
15496650	3580756	wkdt	Q27347360						
15969324	3580756		Bats Cave Beach						
15629774	3580757	wkdt	Q27347359						
15969325	3580757		Barkers						
1456752	3580760		Anchors Point						
4332778	3580760		Anchor Point						
1886586	6299934	icao	MWCG						
5885468	7730680	iata	LYB						
5885469	7730680	icao	MWCL						
5885470	7730680	link	https://en.wikipedia.org/wiki/Edward_Bodden_Airfield						
8574818	7730680	en	Edward Bodden Airfield						
8574826	7730680	en	Little Cayman Airport						
8574987	7730680	fr	Aéroport Edward Bodden						
8604996	8689197		Eastern						
8604997	8689197		Eastern District						
8604998	8689198		Midland						
8604999	8689198		Midland District						
8605000	8689203		Western						
8605001	8689203		Western District						
13786069	9294258	unlc	KYLYB						
15476473	9350643	wkdt	Q27347353						
10906627	10346796	link	https://en.wikipedia.org/wiki/George_Town,_Cayman_Islands						
10906558	10375968	link	https://en.wikipedia.org/wiki/West_Bay,_Cayman_Islands						
10906559	10375969	link	https://en.wikipedia.org/wiki/Bodden_Town_%28district%29						
10906560	10375970	link	https://en.wikipedia.org/wiki/North_Side,_Cayman_Islands						
10906561	10375971	link	https://en.wikipedia.org/wiki/East_End,_Cayman_Islands						
10906562	10375972	link	https://en.wikipedia.org/wiki/Cayman_Brac						
10906563	10375972		Sister Island						
10906642	10375972		Little Cayman						
10906643	10375972		Cayman Brac						
13786066	10375972	unlc	KYCYB						
17312010	12440247	unlc	KYGEC						
